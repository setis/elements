import {MonoTypeOperatorFunction, Observable, Operator, Subscriber, TeardownLogic} from 'rxjs';

export interface IVHost {
    test(host: string): boolean

    exec(host: string): string | false
}

export class Vhost implements IVHost {
    host: string;

    constructor(
        handler: Host
    ) {
        this.handler(handler);
    }

    static regexp(data: string): RegExp {
        return new RegExp(data.replace(/(\*)/gm, `.*?`).replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'gi');
    }

    handler(value: Host) {
        switch (typeof value) {
            case 'string':
                this.host = value;
                if (value === '*') {
                    this.test = function () {
                        return true;
                    };
                } else {
                    if (/(\*)/gm.test(value)) {
                        const handler = Vhost.regexp(value);
                        this.test = function (value: string) {
                            return handler.test(value);
                        };
                    } else {
                        this.test = function (name: string) {
                            return name === value;
                        };
                    }
                }
                break;
            case 'object':
                if (!(value instanceof RegExp)) {
                    throw new Error(`not found RegExp:${value}`);
                }
                this.host = value.source;
                this.test = function (name: string) {
                    return value.test(name);
                };
                break;
            default:
                throw new Error(`not found typeof: ${typeof value}`);
        }
    }

    toString() {
        return this.host;
    }

    test(host: string): boolean {
        return false;
    }

    exec(host: string): string | false {
        return (this.test(host)) ? this.host : false;
    }
}

export class Vhosts extends Array<Vhost> implements IVHost {
    cache: Map<string, string | false>;

    load(value: Hosts): this {
        for (const vhost of value) {
            this.push(new Vhost(vhost));
        }
        return this;
    }

    exec(host: string, cache: boolean = true): string | false {
        if (cache && this.cache.has(host)) {
            return this.cache.get(host);
        }
        for (const vhost of this) {
            if (vhost.test(host)) {
                if (cache) {
                    this.cache.set(host, vhost.host);
                }
                return vhost.host;
            }
        }
        if (cache) {
            this.cache.set(host, false);
        }
        return false;
    }

    test(host: string, cache: boolean = true): boolean {
        const result = this.exec(host, cache);
        return (typeof result === 'string');
    }

    push(...items): number {
        this.cache.clear();
        return super.push(...items);
    }

    unshift(...items): number {
        this.cache.clear();
        return super.unshift(...items);
    }

    pop(): Vhost | undefined {
        this.cache.clear();
        return super.pop();
    }

    shift(): Vhost | undefined {
        this.cache.clear();
        return super.shift();
    }
}

export type HostName = Host | Hosts;
export type Host = string | RegExp;
export type Hosts = Array<string | RegExp> | Set<string | RegExp>;

export function VirtualHost<T = any>({hostname, host, done}: { hostname?: HostName, host?: (value: T) => string, done?: (value: T, vhost: string) => void, fix?: boolean }): MonoTypeOperatorFunction<T> {
    if (hostname === '*' || !hostname) {
        return (source: Observable<T>): Observable<T> => source;
    }
    return function VirtualHostOperatorFunction(source: Observable<T>): Observable<T> {
        return source.lift(new VirtualHostOperator<T>(hostname, host, done));
    };
}

class VirtualHostOperator<T> implements Operator<T, T> {
    protected handler: IVHost;
    protected host?: (value: any) => string;
    protected done?: (value: T, vhost: string) => void;

    constructor(hostname: HostName, host?: (value: any) => string, done?: (value: T, vhost: string) => void) {
        if (hostname instanceof Array || hostname instanceof Set) {
            this.handler = (new Vhosts()).load(hostname);
        } else if (typeof hostname === 'string' || hostname instanceof RegExp) {
            this.handler = new Vhost(hostname);
        } else {
            throw new Error();
        }

        if (host) {
            this.host = host;
        }
        if (done) {
            this.done = done;
        }
    }

    call(subscriber: Subscriber<T>, source: any): TeardownLogic {
        return source.subscribe(new VirtualHostSubscriber<T>(subscriber, this.handler.exec.bind(this), this.host, this.done));
    }
}

class VirtualHostSubscriber<T> extends Subscriber<T> {

    constructor(
        destination: Subscriber<T>,
        private handler: (host: string) => string | false,
        private host?: (value: any) => string,
        private done?: (value: T, vhost: string) => void
    ) {
        super(destination);
    }

    protected _next(value: any) {
        try {
            const hostname = (this.host) ? this.host(value) : value;
            const result = this.handler(hostname);
            if (result) {
                if (this.done) {
                    this.done(value, hostname);
                }
                this.destination.next(value);
            }
        } catch (err) {
            this.destination.error(err);
            return;
        }

    }
}
