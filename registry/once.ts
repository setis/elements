import {isAbsolute, join, normalize, relative} from 'path';
import {Subject, Subscription} from 'rxjs';

export interface ConfigOnce {
    root: string;
    store: { [path: string]: string };
}

export enum ActionEnum {
    Delete = 'delete',
    Insert = 'insert',
    Change = 'change',
    Set = 'set'
}

export type Notification = {
    action: ActionEnum.Delete,
    link: string,
    name: string
} | {
    action: ActionEnum.Insert
    name: string,
    link: string
} | {
    action: ActionEnum.Change,
    name: string,
    nameOld: string,
    link: string
} | {
    action: ActionEnum.Set,
    root: string,
    rootOld?: string
};

export class Once implements ReadonlyMap<string, string> {
    readonly names: Set<string> = new Set();
    readonly subject: Subject<Notification> = new Subject();
    readonly subscription: Subscription = new Subscription();
    protected store: Map<string, string> = new Map();

    constructor(parameters: { root?: string, store?: { [p: string]: string } }) {
        const {root, store} = parameters;
        if (root) {
            this._root = root;
            if (store) {
                this.load(store);
            }
        }
    }

    get size(): number {
        return this.store.size;
    }

    private _root: string;

    get root(): string {
        return this._root;
    }

    set root(value: string) {
        const old = this._root;
        this._root = value;
        this.subject.next({
            action: ActionEnum.Set,
            root: value,
            rootOld: old,
        });
    }

    forEach(callbackfn: (value: string, key: string, map: ReadonlyMap<string, string>) => void, thisArg?: any): void {
        return this.store.forEach(callbackfn, thisArg);
    }

    get(key: string): string {
        return this.store.get(key);
    }

    onDestroy() {
        this.subscription.unsubscribe();
    }

    absolute(path: string) {
        return (isAbsolute(path)) ? normalize(path) : join(this._root, path);
    }

    relative(path: string) {
        return (isAbsolute(path)) ? relative(this._root, path) : path;

    }

    [Symbol.iterator](): IterableIterator<[string, string]> {
        return this.store[Symbol.iterator]();
    }

    entries(): IterableIterator<[string, string]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<string> {
        return this.store.values();
    }

    set(path: string, name: string,): this {
        const link = this.absolute(path);
        let del;
        if (this.store.has(link)) {
            del = this.store.get(link);
            this.names.delete(del);
        }
        this.store.set(link, name);
        this.names.add(name);

        this.subject.next(del ? {
            action: ActionEnum.Change,
            name,
            nameOld: del,
            link
        } : {
            action: ActionEnum.Insert,
            name,
            link
        });
        return this;
    }

    add(path: string, name: string): this {
        const link = this.absolute(path);
        if (this.store.has(link)) {
            throw new Error(`duplicate name:${name} path:${path}`);
        }
        this.store.set(link, name);
        this.names.add(name);
        this.subject.next({
            action: ActionEnum.Insert,
            name,
            link
        });
        return this;
    }

    remove(value: string) {
        const remove = (name, link) => {
            this.names.delete(name);
            this.store.delete(link);
            this.subject.next({
                action: ActionEnum.Delete,
                name,
                link
            });
        };
        if (this.store.has(value)) {
            remove(this.store.get(value), value);
            return;
        }
        const path = this.absolute(value);
        for (const [name, link] of this.store.entries()) {
            if (path == link) {
                remove(name, link);
                return;
            }
        }
    }

    path(name: string): string | false {
        if (this.store.has(name)) {
            return this.absolute(this.store.get(name));
        }
        return false;
    }

    name(path: string): string | false {
        const link = this.absolute(path);
        for (const [name, link1] of this.store.entries()) {
            if (link1 == link) {
                return name;
            }
        }
        return false;
    }

    load(data: { [name: string]: string }): this {
        Object.keys(data).forEach(value => {
            this.add(data[value], value);
        });
        return this;
    }

    save(absolute: boolean = false): { [path: string]: string } {
        const result = {};
        this.store.forEach((value, key) => {
            result[value] = (absolute) ? this.absolute(key) : key;
        });
        return result;
    }

    toObject(): {
        root: string
        store: { [path: string]: string }
    } {
        return {
            root: this._root,
            store: this.save()
        };
    }

    toJson() {
        return JSON.stringify(this.toObject());
    }

    has(path: string): boolean {
        return this.store.has(path);
    }

    clear() {
        const remove = (name, link) => {
            this.names.delete(name);
            this.store.delete(link);
            this.subject.next({
                action: ActionEnum.Delete,
                name,
                link
            });
        };
        for (const [link, name] of this.store) {
            remove(name, link);
        }
        this.store.clear();
        this.names.clear();
    }

    paths(absolute: boolean = true): string[] {
        return Array.from(this.store.keys())
            .map(value => (absolute) ? value : this.relative(value));
    }
}
