import {Config, ConfigMulti, ConfigOnce, Multi, Once, Type} from './index';

export class Store {
    protected store: Map<string, Type> = new Map();

    [Symbol.iterator](): IterableIterator<[string, Type]> {
        return this.store[Symbol.iterator]();
    }

    entries(): IterableIterator<[string, Type]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<Type> {
        return this.store.values();
    }

    * paths() {
        for (const store of this.store.values()) {
            for (const path of store.keys()) {
                yield path;
            }
        }
    }

    names({includes, excludes}: { includes?: string[], excludes?: string[] }): Set<string> {
        const list: Set<string> = new Set();
        for (const [name, store] of this.store.entries()) {
            if ((includes && includes.includes(name)) || (excludes && excludes.indexOf(name) === -1)) {
                for (const name of store.names) {
                    list.add(name);
                }
            }
        }
        return list;
    }

    create(name: string, value: Config | Type) {
        if (typeof value !== 'object') {
            throw new Error(`ох , бл...`);
        }
        let container: Type;
        if (value instanceof Once || value instanceof Multi) {
            container = value;
        } else {
            const list = ['root', 'store'].filter(value1 => !value.hasOwnProperty(value1));
            if (list.length > 0) {
                throw new Error(`not found ${list.join(',')}`);
            }
            const first = value.store[Object.keys(value.store)[0]];
            switch (typeof first) {
                case 'object':
                    if (!(value instanceof Array)) {
                        throw new Error(`ох , бл...`);
                    }
                    container = new Multi(value as ConfigMulti);
                    break;
                case 'string':
                    container = new Once(value as ConfigOnce);
                    break;
                default:
                    throw new Error(`ох , бл...`);
            }
        }
        this.store.set(name, container);
        return container;
    }

    remove(value: string | Type) {
        switch (typeof value) {
            case 'string':
                this.store.delete(value);
                break;
            case 'object':
                if (!(value instanceof Once) && !(value instanceof Multi)) {
                    throw new Error(`ох , бл...`);
                }
                for (const [name, store] of this.store.entries()) {
                    if (store === value) {
                        this.store.delete(name);
                        return;
                    }
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }
    }

    use(value: string) {
        if (!this.store.has(value)) {
            throw new Error(`ох , бл...`);
        }
        return this.store.get(value);
    }
}
