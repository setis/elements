import {isAbsolute, join, normalize, relative} from 'path';

export interface ConfigMulti {
    root: string;
    store: { [path: string]: string[] };
}

export class Multi {
    readonly names: Set<string> = new Set();
    protected store: Map<string, Set<string>> = new Map();
    protected root: string;

    constructor(value?: ConfigMulti) {
        if (value) {
            const {store, root} = value;
            this.root = root;
            this.load(store);
        }
    }

    has(path: string): boolean {
        return this.store.has(path);
    }

    set(path: string, names: string[]): this {
        const link = this.relative(path);
        if (this.store.has(path)) {
            const store = this.store.get(link);
            store.forEach(value => this.names.delete(value));
            store.clear();
            names.forEach(value => {
                this.names.add(value);
                store.add(value);
            });
            return this;
        }
        this.store.set(link, new Set(names));
        names.forEach(value => this.names.add(value));
        return this;
    }

    add(path: string, names: string[]): this {
        const link = this.relative(path);
        if (this.store.has(link)) {
            const list = names.filter(value => !this.names.has(value));
            if (list.length === 0) {
                return this;
            }
            const store = this.store.get(link);
            list.forEach(value => {
                store.add(value);
                this.names.add(value);
            });
            return this;
        }
        this.store.set(link, new Set(names));
        names.forEach(value => this.names.add(value));
        return this;
    }

    remove(value: string | string[] | Set<string>) {
        let list: string[];
        switch (typeof value) {
            case 'string':
                list = [value];
                break;
            case 'object':
                if (value instanceof Array) {
                    if (value.length === 0) {
                        return;
                    }
                    list = value;
                } else if (value instanceof Set) {
                    if (value.size === 0) {
                        return;
                    }
                    list = Array.from(value);
                } else {
                    throw new Error(`not found type `);
                }
                break;
            default:
                throw new Error(`not found type `);
        }
        const names = new Set(list.filter(value1 => this.names.has(value1)));
        const paths = new Set(list
            .map(value1 => this.relative(value1))
            .filter(value1 => this.names.has(value1))
        );
        if (names.size == 0 && paths.size === 0) {
            return;
        }
        for (const [path, list] of this.store.entries()) {
            if (paths.size > 0) {
                if (paths.has(path)) {
                    list.forEach(value1 => this.names.delete(value1));
                    this.store.delete(path);
                    paths.delete(path);
                }
            }
            if (names.size > 0) {
                const list2 = Array.from(names).filter(value1 => list.has(value1));
                if (list2.length > 0) {
                    list2.forEach(value1 => {
                        list.delete(value1);
                        this.names.delete(value1);
                    });
                }
            }
            if (names.size == 0 && paths.size == 0) {
                return;
            }
        }
    }

    path(name: string): string | false {
        if (this.names.has(name)) {
            for (const [path, names] of this.store.entries()) {
                if (names.has(name)) {
                    return path;
                }
            }
            return false;
        }
        return false;
    }

    name(path: string): Set<string> | false {
        const link = this.relative(path);
        for (const [path, names] of this.store.entries()) {
            if (path == link) {
                return names;
            }
        }
        return false;
    }

    load(data: { [path: string]: string[] }): this {
        Object.keys(data).forEach(value => {
            this.add(value, data[value]);
        });
        return this;
    }

    paths(absolute: boolean = true): string[] {
        return Array.from(this.store.keys())
            .map(value => (absolute) ? this.absolute(value) : value);
    }

    save(absolute: boolean = false): { [path: string]: string[] } {
        const result = {};
        this.store.forEach((value, key) => {
            result[(absolute) ? this.absolute(key) : key] = Array.from(value);
        });
        return result;
    }

    toObject(): {
        root: string
        store: { [path: string]: string[] }
    } {
        return {
            root: this.root,
            store: this.save()
        };
    }

    toJson() {
        return JSON.stringify(this.toObject());
    }

    absolute(path: string) {
        return (isAbsolute(path)) ? normalize(path) : join(this.root, path);
    }

    relative(path: string) {
        return (isAbsolute(path)) ? relative(this.root, path) : path;

    }

    [Symbol.iterator](): IterableIterator<[string, Set<string>]> {
        return this.store[Symbol.iterator]();
    }

    entries(): IterableIterator<[string, Set<string>]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<string> {
        return this.names.values();
    }

    clear() {
        this.store.clear();
        this.names.clear();
    }
}
