import {ConfigMulti, Multi} from './multi';
import {ConfigOnce, Once} from './once';

export {Multi as RegistryMulti, ConfigMulti as RegistryConfigMulti} from './multi';
export {Once as RegistryOnce, ConfigOnce as RegistryConfigOnce} from './once';
export * from './multi';
export * from './once';
export type Type = Once | Multi;
export type Config = ConfigOnce | ConfigMulti;

export function factory(type: 'once', config?: ConfigOnce): Once;

export function factory(type: 'multi', config?: ConfigMulti): Multi;

export function factory(type: 'once' | 'multi', config?: Config): Type {
    switch (type) {
        case 'multi':
            return new Multi(config as ConfigMulti);
        case 'once':
            return new Once(config as ConfigOnce);
        default:
            throw new Error(`ох , бл...`);
    }
}

export {Store as RegistryStore} from './store';
