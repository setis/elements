import {Type} from "./listener";

/**
 *
 *
 * @param self
 * @param parameters
 *
 */
export async function emit(self: Type, parameters: { context: any, call: string, args?: any[], sync: boolean, throwError: boolean, detail: boolean }) {
    const {context, call, args, sync = true, throwError = false, detail = false} = parameters;
    const collection = [];
    const result = {};
    let i = 0;
    for (const listener of self.listener(call)) {
        i++;
        collection.push(async () => {
            try {
                const data = await listener.apply(context, args);
                if (detail) {
                    result[i] = {
                        listener,
                        status: true,
                        result: data,
                    }
                } else {
                    return data
                }
            } catch (e) {
                if (throwError) {
                    throw e;
                }
                if (detail) {
                    result[i] = {
                        listener,
                        status: false,
                        error: e,
                    }
                }
            }
        });
    }
    if (sync) {
        const result = [];
        for (const chunk of collection) {
            const data = await chunk();
            result.push(data);
        }
        if (!detail) {
            return result;
        }
    } else {
        const result = await Promise.all(collection);
        if (!detail) {
            return result;
        }
    }
    return result;
}
