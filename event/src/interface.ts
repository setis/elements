export enum EnumMode {
    /**
     * @description только один обработчик на  событие
     */
    Once,
    /**
     * @description несколько обработчиков на событие , могут и повторяущиеся
     */
    Multi,
    /**
     * @description несколько обработчиков на событие , но уникальные
     */
    Uniq
}

export enum EnumPriority {
    /**
     * @description родитель
     */
    Src,
    /**
     * @description ребенок наследуемый
     */
    Dst

}

export enum EnumMatch {
    /**
     * @description найти любой src или dst
     */
    Any,
    /**
     * @description найти все src и dst
     */
    All,
    /**
     * @description найти все src и dst но только уникальные(не повторяющиеся) события
     */
    Uniq
}

export interface IListener {
    on(name: string, listener: Function);

    off(name: string);

    off(name: string, listener: Function);

    events(): IterableIterator<string>;

    listener(name: string): Array<Function>;

    listener(): { [name: string]: Array<Function> };
}

export interface IEmitter {

    emit();
}

export type Data = DataEvent | DataQueue

export interface DataEvent {
    name: string
}

export interface DataQueue extends DataEvent {
    sync: boolean,
    queue: Data[]
}
