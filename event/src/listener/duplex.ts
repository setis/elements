import {Type} from "./index";
import {EnumMatch, EnumMode, EnumPriority} from "../interface";

export class Duplex {
    src: Type;
    dst: Type;
    mode: EnumMode;
    priority: EnumPriority;
    match: EnumMatch;
    lock: boolean = false;

    * events(): IterableIterator<string> {
        const uniq = new Set();
        for (const container of [this.dst, this.src]) {
            for (const event of container.events()) {
                if (uniq.has(event)) {
                    uniq.add(event);
                    yield event;
                }
            }
        }
    }

    on(name: string, ...args): this {
        if (this.lock) {
            throw new Error(`запрященно запись`)
        }
        // @ts-ignore
        if (typeof this.dst.on !== "function") {
            throw new Error(`не возможности установить обработчик на событие`)
        }
        // @ts-ignore
        this.dst.on(name, ...args)
        return this;
    }

    off(name: string, ...args): this {
        if (this.lock) {
            throw new Error(`запрященно запись`)
        }
        // @ts-ignore
        if (typeof this.dst.off !== "function") {
            throw new Error(`не возможности удалить обработчик на событие`)
        }
        // @ts-ignore
        this.dst.off(name, ...args)
        return this;
    }

    has(name: string): boolean {
        for (const container of [this.dst, this.src]) {
            if (container.has(name)) {
                return true;
            }
        }
        return false;
    }

    * listeners(): IterableIterator<[string, IterableIterator<Function>]> {
        for (const event of this.events()) {
            yield [event, this.listener(event)]
        }
    }

    * listener(name: string): IterableIterator<Function> {
        const a = (this.priority === EnumPriority.Src) ? this.src : this.dst;
        const b = (this.priority === EnumPriority.Dst) ? this.dst : this.src;
        if (a === b) {
            return a.listener(name);
        }
        const f1 = a.has(name);
        const f2 = b.has(name);
        if (!f1 && !f2) {
            return;
        }
        if (!f1 && f2) {
            return b.listener(name);
        }
        if (f1 && !f2) {
            return a.listener(name);
        }
        switch (this.match) {
            case EnumMatch.Any:
                return a.listener(name);
            case EnumMatch.All: {
                for (const listener of a.listener(name)) {
                    yield listener;
                }
                for (const listener of b.listener(name)) {
                    yield listener;
                }
            }
                break;
            case EnumMatch.Uniq: {
                const uniq = new Set();
                for (const listeners of [a.listener(name), b.listener(name)]) {
                    for (const listener of listeners) {
                        if (uniq.has(listener)) {
                            uniq.add(listener);
                            yield listener;
                        }
                    }
                }
            }
                break;
            default:
                throw new Error();
        }
    }


}
