import {Uniq} from "./uniq";
import {Once} from "./once";
import {Multi} from "./multi";
import {Bridge} from "./bridge";
import {Duplex} from "./duplex";

export type Type = Uniq | Once | Multi | Bridge | Duplex;
export * from './bridge'
export * from './multi'
export * from './once'
export * from './uniq'
export * from './duplex'
