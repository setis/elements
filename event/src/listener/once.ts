import {EnumMode} from "../interface";
import {emit} from "../emit";

export class Once {
    readonly store: Map<string, Function> = new Map();
    readonly mode: EnumMode = EnumMode.Once;

    events(): IterableIterator<string> {
        return this.store.keys();
    }

    has(name: string): boolean {
        return this.store.has(name);
    }

    listeners(): IterableIterator<[string, Function]> {
        return this.store.entries();
    }

    * listener(name: string): IterableIterator<Function> {
        if (this.store.has(name)) {
            yield this.store.get(name);
        }
    }

    on(name: string, listener: Function): this {
        if (typeof listener !== "function") {
            throw new Error(`${name} ввод данныж не является функцией`)
        }
        this.store.set(name, listener);
        return this;
    }

    off(name: string): this {
        if (this.store.has(name)) {
            this.store.delete(name);
        }
        return this;
    }

    emit(parameters: { context: any, call: string, args?: any[], sync: boolean, throwError: boolean, detail: boolean }) {
        return emit(this, parameters);
    }
}
