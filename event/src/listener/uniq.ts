import {EnumMode} from "../interface";
import {emit} from "../emit";

export class Uniq {
    readonly store: Map<string, Set<Function>> = new Map();
    readonly mode: EnumMode = EnumMode.Uniq;

    events(): IterableIterator<string> {
        return this.store.keys();
    }

    has(name: string): boolean {
        return this.store.has(name);
    }

    * listeners(): IterableIterator<[string, IterableIterator<Function>]> {
        for (const [event, listeners] of this.store) {
            for (const listener of listeners) {
                yield [event, listeners.values()]
            }
        }
    }

    * listener(name: string): IterableIterator<Function> {
        if (this.store.has(name)) {
            for (const listener of this.store.get(name)) {
                yield listener;
            }
        }
    }

    on(name: string, ...listeners: Array<Function>): this {
        if (listeners.length === 0) {
            throw new Error(`${name} не введен listener`)
        }
        if (listeners.length !== listeners.filter(value => typeof value === "function").length) {
            throw new Error(`${name} не все явялются функцией`)
        }
        const container = (this.store.has(name)) ? this.store.get(name) as Set<Function> : new Set();
        if (!this.store.has(name)) {
            this.store.set(name, container);
        }
        listeners.forEach(value => container.add(value));
        return this;
    }

    off(name: string): this;
    off(name: string, listeners: Array<Function>): this;
    off(name: string, listeners?: Array<Function>): this {
        if (!this.store.has(name)) {
            return this;
        }
        if (listeners.length === 0) {
            this.store.delete(name)
        }
        if (listeners.length !== listeners.filter(value => typeof value === "function").length) {
            throw new Error(`${name} не все явялются функцией`)
        }
        const container = this.store.get(name);
        if (container.size > 0) {
            listeners.filter(value => container.has(value)).forEach(value => container.delete(value))
            if (container.size === 0) {
                this.store.delete(name);
            }
        }
        return this;
    }

    emit(parameters: { context: any, call: string, args?: any[], sync: boolean, throwError: boolean, detail: boolean }) {
        return emit(this, parameters);
    }

}
