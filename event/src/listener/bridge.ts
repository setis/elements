import {Type} from "./index";
import {emit} from "../emit";

export class Bridge {
    readonly store: Set<Type> = new Set();
    uniq: boolean = true;

    * events(): IterableIterator<string> {
        const uniq = new Set();
        for (const container of this.store.values()) {
            for (const event of container.events()) {
                if (uniq.has(event)) {
                    uniq.add(event);
                    yield event;
                }
            }
        }
    }

    has(name: string): boolean {
        for (const container of this.store.values()) {
            if (container.has(name)) {
                return true;
            }
        }
        return false;
    }

    * listeners(uniq: boolean = this.uniq): IterableIterator<[string, IterableIterator<Function>]> {
        for (const event of this.events()) {
            yield [event, this.listener(event, uniq)]
        }
    }

    * listener(name: string, uniq: boolean = this.uniq): IterableIterator<Function> {
        const collection = new Set();
        for (const store of this.store.values()) {
            for (const listener of store.listener(name)) {
                if (uniq) {
                    if (collection.has(listener)) {
                        collection.add(listener);
                        yield listener
                    }
                } else {
                    yield listener
                }
            }
        }
    }

    emit(parameters: { context: any, call: string, args?: any[], sync: boolean, throwError: boolean, detail: boolean }) {
        return emit(this, parameters);
    }
}
