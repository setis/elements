import uuid = require("uuid");
import {Data} from "./interface";

export class Meta extends Array<Meta> {
    readonly uuid: string = uuid();

    constructor(
        readonly name: string,
        queue: Array<Meta> = [],
        public sync: boolean = true
    ) {
        super();
        if (queue) {
            queue.forEach(value => this.push(value))
        }
    }

    findName(name: string, position: number = 0) {
        let i = 0;
        for (const meta of this) {
            if (meta.name === name) {
                if (i === position) {
                    return meta
                }
                i++;
            }
        }
        throw new Error(`${this.toString()} name: ${name} position: ${position}`)
    }

    findUuid(uuid: string) {
        for (const meta of this) {
            if (meta.uuid === uuid) {
                return meta
            }
        }
        throw new Error(`${this.toString()}  uuid: ${uuid}`)
    }

    * uids() {
        for (const container of this) {
            yield container.uuid
        }
    }

    * events() {
        for (const container of this) {
            yield container.name
        }
    }

    toString() {
        return `${this.uuid}://${this.name}${(this.length > 0) ? '-' + (this.sync ? 'group' : 'collection') : ''}`;
    }

    toObject(): Data {
        if (this.length === 0) {
            return {
                name: this.name
            }
        }
        return {
            name: this.name,
            sync: this.sync,
            queue: [...this]
        }
    }
}
