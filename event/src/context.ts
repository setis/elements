import {Meta} from "./meta";
import {Bridge, Multi, Once, Type, Uniq} from "./listener";
import {emit} from './emit'

export class Context {
    begin?: Date;
    end?: Date;
    protected children: Set<Context>;

    constructor(
        readonly meta: Meta,
        readonly parent?: Context
    ) {
        if (!this.parent) {
            this._data = new Object();
            this._args = new Map();
            if (this.meta.length > 0) {
                this.children = new Set();
            }
            this._stop = false;
        }
    }

    protected _stop: boolean;

    get stop() {
        return (this.parent) ? this.parent.stop : this._stop;
    }

    set stop(value: boolean) {
        if (this.stop === value) {
            return;
        }
        if (this.parent) {
            this.parent.stop = value;
        } else {
            this._stop = value;
        }
    }

    protected _event: Type;

    get event(): Type {
        return (this.parent) ? this.parent.event : this._event;
    }

    set event(value: Type) {
        if (this.parent) {
            throw new Error(`нельзя установить так как в дочернем`)
        }
        if (
            !(value instanceof Uniq
                || value instanceof Once
                || value instanceof Multi
                || value instanceof Bridge)
        ) {
            throw new Error(`может быть только Uniq,Once,Multi,Bridge`)
        }
        this._event = value;

    }

    protected _data: Object;

    get data(): Object {
        return (this.parent) ? this.parent.data : this._data;
    }

    protected _args: Map<string, any>;

    get args(): Map<string, any> {
        return (this.parent) ? this.parent.args : this._args;
    }

    get total() {
        if (!this.end || !this.begin) {
            return;
        }
        return this.end.getTime() - this.begin.getTime();
    }

    inject(meta: Meta) {
        if (this.args.has(meta.uuid)) {
            return this.args.get(meta.uuid);
        }
        if (this.args.has(meta.name)) {
            return this.args.get(meta.name);
        }
    }

    emit(parameters: { call: string, args?: any[], sync: boolean, throwError: boolean, detail: boolean }) {
        if (this.stop) {
            return;
        }
        return emit(this.event, {
            ...parameters,
            context: this
        });
    }

    async dispatch(...args) {
        if (this.stop) {
            return;
        }
        this.begin = new Date();
        if (this.meta.length > 0) {
            const {sync} = this.meta;
            const collection = [];
            if (!this.children) {
                this.children = new Set();
            }
            for (const event of this.meta) {
                const container = new Context(event, this);
                this.children.add(container);
                const args1 = this.inject(event) || args;
                if (sync) {
                    await container.dispatch(...args1);
                } else {
                    collection.push(container.dispatch(...args1))
                }
            }
            if (!sync) {
                await Promise.all(collection);
            }
        } else {
            await emit(this.event, {
                context: this,
                sync: this.meta.sync,
                call: this.meta.name,
                args,
                throwError: true,
                detail: false
            })
        }
        this.end = new Date();
        if (this.parent) {
            this.parent.children.delete(this);
        }
    }

}
