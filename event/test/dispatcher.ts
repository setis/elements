import {assert, expect} from "chai";
import {Context, Meta, Once} from "../src";

describe('dispatcher', function () {
    it('проверка очереди если одного всписке собития не существует', function () {

        const context = new Context(new Meta('start', [
            new Meta('begin'),
            new Meta('asd'),
            new Meta('end'),
            new Meta('asdf')
        ]));
        const event = new Once();
        event
            .on('begin', function () {
                this.data.start = process.hrtime();
            })
            .on('end', function () {
                return Promise.resolve();
            });
        context.event = event;
        assert.equal(context.event, event);
        return context.dispatch();
    });
    it.skip('ошибка запуск не существующий списка очереди', function () {
        const context = new Context(new Meta('start', [
            new Meta('begin'),
            new Meta('end')
        ]));
        const event = new Once();
        context.event = event;
        expect(async () => {
            context.dispatch()
        }).to.throw(`уже существует такой список очереди start`);
    })
    it('ввод данных и получение результата', function () {
        const context = new Context(new Meta('start', [
            new Meta('begin'),
            new Meta('sum'),
            new Meta('assert'),
            new Meta('end'),
        ]));
        const event = new Once();
        context.event = event;
        event
            .on('begin', function (i) {
                assert.equal(i, 2);
                this.data.i = i;
            })
            .on('sum', function () {
                this.data.i += 2
            })
            .on('assert', function () {
                assert.equal(this.data.i, 4, 'assert 4');
            })
            .on('end', function () {
                return Promise.resolve();
            });
        context.args.set('begin', [2])
        return context.dispatch(2);
    });
    it('проверка асинхроная группа событий', function () {
        const context = new Context(new Meta('start', [
            new Meta('begin'),
            new Meta('sum'),
            new Meta('assert'),
            new Meta('event', [
                new Meta('event1'),
                new Meta('event2'),
                new Meta('assert'),
            ], false),
            new Meta('end'),
        ]));
        const event = new Once();
        context.event = event;
        event
            .on('begin', async function (i) {
                assert.equal(i, 2);
                this.data.i = 2;
                this.data.start = process.hrtime();
            })
            .on('sum', async function () {
                this.data.i += 2;
            })
            .on('assert', function () {
                assert.equal(this.data.i, 4, 'assert 4');
            })
            .on('event1', async function () {
                await new Promise(resolve => {
                    setTimeout(resolve, 5);
                });
                this.data.event1 = new Date().getTime();
            })
            .on('event2', async function () {
                await new Promise(resolve => {
                    setTimeout(resolve, 1);
                });
                this.data.event2 = new Date().getTime();
            })
            .on('end', async function () {
                assert.equal((this.data.event2 - this.data.event1) < 0, true);
            });

        expect(context.meta.map(value => value.name)).to.include.members([
            'begin',
            'sum',
            'assert',
            'event',
            'end'
        ])
        expect(context.meta.findName('event').map(value => value.name)).to.include.members([
            'event1', 'event2', 'assert'
        ])
        context.args.set('begin', [2])
        return context.dispatch(2);
    });
    it('проверка группу событий', async function () {
        const context = new Context(new Meta('start', [
            new Meta('begin'),
            new Meta('sum'),
            new Meta('assert'),
            new Meta('event', [
                new Meta('event1'),
                new Meta('event2'),
                new Meta('assert'),
            ]),
            new Meta('end'),
        ]));
        const event = new Once();
        context.event = event;
        event
            .on('begin', function (i) {
                this.data.i = i;
            })
            .on('sum', function () {
                this.data.i += 2;
            })
            .on('assert', function () {
                assert.equal(this.data.i, 4, 'assert');
            })
            .on('event1', async function () {
                await new Promise(resolve => {
                    setTimeout(resolve, 15);
                });
                this.data.event1 = (new Date()).getTime();

            })
            .on('event2', async function (callback) {
                await new Promise(resolve => {
                    setTimeout(resolve, 1);
                });
                this.data.event2 = (new Date()).getTime();
            })
            .on('end', function () {
                assert.equal((this.data.event2 - this.data.event1) > 0, true);

            });
        expect(context.meta.map(value => value.name)).to.include.members([
            'begin',
            'sum',
            'assert',
            'event',
            'end'
        ])
        expect(context.meta.findName('event').map(value => value.name)).to.include.members([
            'event1', 'event2', 'assert'
        ])
        context.args.set('begin', [2])
        return context.dispatch();
    });
    it('проверка списка очереди', function () {
        const context = new Context(new Meta('start', [
            new Meta('begin'),
            new Meta('sum'),
            new Meta('collection', [
                new Meta('collection:event1'),
                new Meta('collection:event2'),
            ], false),
            new Meta('collection:assert'),
            new Meta('group', [
                new Meta('group:event1'),
                new Meta('group:event2'),
                new Meta('group:assert'),
            ]),
            new Meta('end'),
        ]));
        const event = new Once();
        context.event = event;
        event
            .on('begin', function (i) {
                assert.equal(i, 2);
                this.data.i = i;

            })
            .on('sum', function () {
                this.data.i += 2;
            })
            .on('group:assert', async function () {
                assert.equal(this.data.i, 4);
                assert.equal((this.data.group_event2 - this.data.group_event1) > 0, true);
            })
            .on('collection:assert', function () {
                assert.equal(this.data.i, 4);
                assert.equal((this.data.collection_event1 - this.data.collection_event2) > 0, true);

            })
            .on('group:event1', async function () {
                await new Promise(resolve => {
                    setTimeout(resolve, 5);
                })
                this.data.group_event1 = (new Date()).getTime();
            })
            .on('group:event2', async function (callback) {
                await new Promise(resolve => {
                    setTimeout(resolve, 1);
                })
                this.data.group_event2 = (new Date()).getTime();
            })
            .on('collection:event1', async function (callback) {
                await new Promise(resolve => {
                    setTimeout(resolve, 5);
                })
                this.data.collection_event1 = (new Date()).getTime();

            })
            .on('collection:event2', async function () {
                await new Promise(resolve => {
                    setTimeout(resolve, 1);
                })
                this.data.collection_event2 = (new Date()).getTime();
            })
            .on('end', function () {
                return new Promise(resolve => {
                    setTimeout(resolve, 1);
                })
            });
        expect(context.meta.map(value => value.name)).to.include.members([
            'begin',
            'sum',
            'collection',
            'collection:assert',
            'group',
            'end',
        ])
        expect(context.meta.findName('group').map(value => value.name)).to.include.members([
            'group:event1', 'group:event2', 'group:assert'
        ]);
        expect(context.meta.findName('collection').map(value => value.name)).to.include.members([
            'collection:event1', 'collection:event2'
        ])
        context.args.set('begin', [2])
        return context.dispatch();
    });
});
