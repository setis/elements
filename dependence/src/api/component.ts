import {Meta, MetaApi, MetaElement} from "../meta";
import {Kernel} from "../kernel";
import {Base} from "./base";
import logger from "../logger";
import {OnDestroy, OnInit} from "../interface";
import {fromEvent, Subscription} from "rxjs";
import {EnumStrategy, Module} from "./module";
import Timer = NodeJS.Timer;

export class Component extends Base implements OnInit, OnDestroy {
    readonly subscription: Subscription = new Subscription();
    public change: boolean = true;
    protected _module_subscription?: Subscription;
    protected _module_destroy?: any;
    protected _timer: Timer;
    protected timeout: number = 1e3;

    constructor(
        readonly kernel: Kernel,
        readonly meta: Meta,
        strategy: EnumStrategy = EnumStrategy.Change
    ) {
        super();
        this.strategy = strategy;
    }

    protected _strategy: EnumStrategy;

    get strategy(): EnumStrategy {
        return this._strategy;
    }

    set strategy(value: EnumStrategy) {
        if (this.strategy === value) {
            return;
        }
        switch (value) {
            case EnumStrategy.Change:
                this.offTimer();
                (async () => {
                    await this.offChange();
                    await this.onChange();
                })();
                break;
            case EnumStrategy.Timer:
                this.offChange();
                this.onTimer();
                break;
            case EnumStrategy.None:
                this.offChange();
                this.offTimer();
                break;
            default:
                throw new Error();
        }
        this._strategy = value;
    }

    protected _module?: Module;

    get module(): Module {
        return this._module;
    }

    set module(value: Module) {
        if (this._module === value) {
            return;
        }
        if (value === undefined && this._module instanceof Module) {
            this.removeModule();
        } else if (value instanceof Module) {
            this.removeModule();
            this._module = value;
            this.addModule();
        } else {
            throw  new Error();
        }
    }

    get id() {
        return this.meta.id;
    }

    addModule() {
        this._module_subscription = fromEvent<boolean>(this._module.emitter, 'state').subscribe(value => this.state = value);
        this.subscription.add(this._module_subscription);
        this._module_destroy = this.removeModule.bind(this);
        this._module.emitter.once('destroy', this._module_destroy);

    }

    removeModule() {
        if (this._module_subscription) {
            this.subscription.remove(this._module_subscription);
            this._module_subscription.unsubscribe();
            delete this._module_subscription;
        }
        if (this._module) {
            if (this._module_destroy) {
                this._module.emitter.off('destroy', this._module_destroy);
                delete this._module_destroy;
            }
            delete this._module;
        }
    }

    async onDestroy() {
        this.offTimer();
        await this.offChange();
        this.removeModule();
        this.emitter.emit('destroy');
        this.subscription.unsubscribe();

    }

    isModule(): boolean {
        return (this._module !== undefined)
    }

    async onInit() {
        this.name()
        await this.check();
        this.onChange();
    }

    async onChange() {
        try {
            const name = this.name();
            const rule = this.kernel.rule(`${name}.install`);
            await rule(this);
        } catch (e) {
            logger.error(e);
            this.change = false;
        }
    }

    async offChange() {
        try {
            const name = this.name();
            const rule = this.kernel.rule(`${name}.uninstall`);
            await rule(this);
        } catch (e) {
            logger.error(e);
            this.change = false;
        }
    }


    name() {
        let name: string;
        if (this.meta instanceof MetaElement) {
            return 'element';
        }
        if (this.meta instanceof MetaApi) {
            return 'api'
        }
        throw new Error()
    }

    async check() {
        try {
            const name = this.name();
            const rule = this.kernel.rule(name);
            this.state = await rule(this);
        } catch (e) {
            this.state = false;
            logger.error(this.id, e)
        }
    }

}
