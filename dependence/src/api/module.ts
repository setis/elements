import {Base} from "./base";
import {createHash} from "crypto";
import {Component} from "./component";
import {Kernel} from "../kernel";
import {fromEvent, Subject, Subscription} from "rxjs";
import {OnDestroy, OnInit} from "../interface";
import {debounceTime, filter} from "rxjs/operators";

export enum EnumStrategy {

    Change = 'change',   // отслеживает изменения зависимостей
    Timer = 'timer',    // через интервал времени делается проверка
    None = 'none'       // проверка делаться только вручную или при изменения
}

export class Module extends Base implements OnInit, OnDestroy {
    readonly store: Map<string, Component> = new Map();
    readonly kernel: Kernel;
    /**
     * если отслеживается событием то не нужно запускать check()
     */

    readonly subscription: Subscription = new Subscription();
    protected readonly _subject_change: Subject<boolean> = new Subject();
    protected hash?: string;
    protected _store_change: Map<Component, [Subscription, Function]> = new Map();

    constructor(
        readonly parameters: { dependencies: Array<string>, lock?: boolean, timout?: number, strategy?: EnumStrategy },
    ) {
        super();

    }

    private _lock: boolean = false;

    get lock(): boolean {
        return this._lock;
    }

    set lock(value: boolean) {
        if (typeof value !== "boolean") {
            throw new Error()
        }
        if (value === value) {
            return;
        }
        this._lock = value;
        this.emitter.emit('lock')
    }

    protected _strategy: EnumStrategy;

    get strategy(): EnumStrategy {
        return this._strategy;
    }

    set strategy(value: EnumStrategy) {
        if (this.strategy === value) {
            return;
        }
        switch (value) {
            case EnumStrategy.Change:
                this.offTimer();
                this.offChange();
                this.onChange();
                break;
            case EnumStrategy.Timer:
                this.offChange();
                this.onTimer();
                break;
            case EnumStrategy.None:
                this.offChange();
                this.offTimer();
                break;
            default:
                throw new Error();
        }
        this._strategy = value;
    }

    /**
     * @description уникальный индефикатор генерирется на оснавания зависимостей
     */
    get id() {
        if (!this.hash) {
            const data = Array.from(this.store.keys()).sort().join(',');
            this.hash = (data.length > 64) ? createHash('sha256').update(data).digest("latin1") : data;
        }
        return `${this.lock ? '0' : '1'}:${this.store.size}#{${this.hash}}`;
    }

    onDestroy() {
        this.emitter.emit('destroy');
        this.clear();
        this.subscription.unsubscribe();
    }

    async onInit() {
        this.emitter
            .on('add', (component: Component) => {
                this.emitter.emit('update');

            })
            .on('delete', args => this.emitter.emit('update'))
            .on('clear', args => this.emitter.emit('update'))
            .on('update', () => {
                delete this.hash;
                this.check();
            })
            .on('lock', value => this.emitter.emit('update'))

        const {dependencies, lock = false, strategy = EnumStrategy.Change, timout = 1e3} = this.parameters;
        this.subscription
            .add(
                this._subject_change
                    .pipe(
                        filter(value => value === true),
                        debounceTime(75)
                    )
                    .subscribe(value => this.check())
            )
            .add(
                this._subject_change
                    .pipe(
                        filter(value => value === false)
                    )
                    .subscribe(value => this.state = false)
            );
        this.timeout = timout;
        this.strategy = strategy;
        await this.load(dependencies);
        this.lock = lock;
        await this.check();
    }

    dependencies() {
        return this.store.keys();
    }

    onChange() {
        for (const component of this.store.values()) {
            this.addChange(component);
        }
        return this;
    }

    offChange() {
        for (const component of this._store_change.keys()) {
            this.removeChange(component);
        }
        return this;
    }

    addChange(component: Component) {
        const subscription = fromEvent(component.emitter, 'state').subscribe(this._subject_change);
        this.subscription.add(subscription);
        const func = this.removeChange.bind(this, component);
        component.emitter.once('destroy', func);
        this._store_change.set(component, [subscription, func]);
        return this;
    }

    removeChange(component: Component) {
        if (!this._store_change.has(component)) {
            return;
        }
        const [subscription, func] = this._store_change.get(component);
        // @ts-ignore
        component.emitter.off('destroy', func);
        this.subscription.remove(subscription);
        subscription.unsubscribe();
        this._store_change.delete(component);
        return this;
    }

    async load(value: Iterable<string>) {
        const tasks = [];
        for (const name of value) {
            tasks.push(this.insert(name));
        }
        await Promise.all(tasks);
    }

    async unload(value: Iterable<string>) {
        const tasks = [];
        for (const name of value) {
            tasks.push(this.remove(name))
        }
        await Promise.all(tasks);
    }

    clear() {
        if (this.lock) {
            throw new Error(`заблокированно изменение не возможно удаление зависимостей id: ${this.id}`);
        }
        for (const [name, component] of this.store.entries()) {
            this.store.delete(name);
            if (this._strategy === EnumStrategy.Change) {
                this.removeChange(component);
            }
            this.emitter.emit('delete', component);
        }
    }

    check() {
        for (const component of this.store.values()) {
            if (!component.state) {
                return false;
            }
        }
        return true;
    }

    async insert(value: string) {
        if (!this.store.has(value)) {
            return;
        }
        if (this.lock) {
            throw new Error(`заблокированно изменение не возможно добавить зависимость: ${value} id: ${this.id}`);
        }
        const component = await this.kernel.useComponent(value);
        this.store.set(value, component);
        if (this.strategy === EnumStrategy.Change) {
            this.addChange(component);
        }
        this.emitter.emit('add', component);
    }

    async remove(value: string) {
        if (!this.store.has(value)) {
            return;
        }
        if (this.lock) {
            throw new Error(`заблокированно изменение не возможно удаление зависимость: ${value} id: ${this.id}`);
        }
        const component = this.store.get(value);
        this.store.delete(value);
        if (this.strategy === EnumStrategy.Change) {
            this.removeChange(component);
        }
        this.emitter.emit('delete', component);
    }


}
