import {Component} from "./component";
import {Module} from "./module";

export {Module as ApiModule} from './module'
export {Component as ApiComponent} from './component'
export type Api = Component | Module;
