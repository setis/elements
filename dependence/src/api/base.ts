import {EventEmitter} from "events";
import {fromEvent, merge, throwError, timer} from "rxjs";
import {filter, switchMap, take} from "rxjs/operators";
import logger from "../logger";
import Timeout = NodeJS.Timeout;

export abstract class Base {
    readonly emitter: EventEmitter = new EventEmitter();
    abstract id: string;
    protected _timer: Timeout;
    protected timeout: number = 1e3;

    protected _state: boolean = false;

    get state(): boolean {
        return (!this._state) ? false : this._state;
    }

    set state(value: boolean) {
        if (typeof value !== 'boolean') {
            throw new Error(`ох , бл...`);
        }
        if (this._state === value) {
            return;
        }
        this._state = value;
        this.emitter.emit('state', value);
    }

    /**
     * @description ожидание когда удовлетворить все потребности
     * @param timeout лимит ожидание по умолчанию 10 секунд
     */
    async wait(state: boolean, timeout?: number) {
        if (this.state === state) {
            return;
        }
        if (typeof timeout === "number") {
            return merge(
                fromEvent(this.emitter, 'state')
                    .pipe(
                        filter(value => value === state),
                    ),
                timer(timeout)
                    .pipe(
                        switchMap(value =>
                            throwError(new Error(`ожидал ${state} перевышен лимит ожидание ${timeout} ms id: ${this.id} time: ${new Date().getTime()}`))
                        )
                    )
            )
                .pipe(
                    take(1)
                )
                .toPromise();
        }
        return fromEvent(this.emitter, 'state')
            .pipe(
                filter(value => value === state),
                take(1),
            )
            .toPromise();
    }

    async up(timeout?: number) {
        return this.wait(true, timeout)
    }

    async down(timeout?: number) {
        return this.wait(false, timeout)
    }

    abstract check();

    onTimer(): this {
        if (!this._timer) {
            this._timer = setInterval(async () => {
                try {
                    await this.check();
                } catch (e) {
                    logger.error(e);
                    this.state = false;
                }
            }, this.timeout)

        }
        return this;
    }

    offTimer(): this {
        if (this._timer) {
            clearInterval(this._timer);
            delete this._timer;
        }
        return this;
    }
}
