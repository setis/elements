import {createHash} from 'crypto';
import {difference} from 'lodash';
import {ApiComponent, ApiModule} from './api';
import {Meta, MetaApi, MetaElement} from './meta';
import {Rule} from "./interface";

export class Kernel {
    /**
     * @description  /^[0|1]?:\d+#{.*?}$/gmi
     * @example 0:23#{b1ebafcd0def5f5a44cbb1c104fcd8c979262821d87a7e3ecc20f3dc844d10ac}
     */
    readonly modules: Set<ApiModule> = new Set();
    /**
     * @example /http/server::service://http.core@1
     * @example /http/server
     */
    readonly components: Map<string, ApiComponent> = new Map();
    readonly metas: Map<string, Meta> = new Map();
    readonly rules: Map<string, Rule> = new Map();

    constructor(
        rules?: { [rule: string]: Rule }
    ) {
        if (rules) {
            for (const rule in rules) {
                const func = rules[rule];
                if (typeof func !== "function") {
                    throw new Error(`rule: ${rule}`)
                }
                this.rules.set(rule, func);
            }
        }
    }

    static mid(dependencies: string[], lock: boolean = false): string {
        const uniq = new Set(dependencies);
        const data = Array.from(uniq).sort().join(',');
        const hash = (data.length > 64) ? createHash('sha256').update(data).digest('latin1') : data;
        return `${lock ? '0' : '1'}:${uniq.size}#{${hash}}`;
    }

    rule(name: string) {
        if (!this.rules.has(name)) {
            throw new Error(`not found rule: ${name}`)
        }
        return this.rules.get(name)
    }

    useMeta(value: string) {
        if (this.metas.has(value)) {
            return this.metas.get(value);
        }
        let meta: Meta;
        if (MetaApi.isValid(value)) {
            meta = new MetaApi(value);
        } else if (MetaElement.isValid(value)) {
            meta = new MetaElement(value)
        } else {
            throw new Error();
        }
        this.metas.set(value, meta);
        return meta;
    }

    useModule(dependencies: string[], lock: boolean, strategy: string = "all") {
        const method = `strategy_module_${strategy}`;
        if (typeof this[method] !== "function") {
            throw new Error(`not found method: ${method} strategy: ${strategy}`)
        }
        const check = this[method](dependencies, lock);
        for (const module of this.modules) {
            if (check(module)) {
                return module;
            }
        }
        const module = new ApiModule({
            dependencies,
            lock
        });
        this.modules.add(module);
        return module;
    }

    async useComponent(value: string): Promise<ApiComponent> {
        if (this.components.has(value)) {
            return this.components.get(value);
        }
        const meta = this.useMeta(value);
        const component = new ApiComponent(this, meta);
        this.components.set(component.id, component);
        await component.onInit();
        return component;
    }

    async removeComponent(component: ApiComponent) {
        if (!this.components.has(component.id)) {
            return;
        }
        this.components.delete(component.id);
        await component.onDestroy();
    }

    protected strategy_module_id(dependencies: string[], lock: boolean) {
        const mid = Kernel.mid(dependencies, lock);
        return (value: ApiModule) => {
            return (value.id === mid)
        }
    }

    protected strategy_module_parameters(dependencies: string[], lock: boolean) {
        return (module: ApiModule) => (module.store.size === dependencies.length && lock === module.lock && difference(dependencies, Array.from(module.dependencies())).length === 0)
    }

    protected strategy_module_all(dependencies: string[], lock: boolean) {
        const list = [
            this.strategy_module_id(dependencies, lock),
            this.strategy_module_parameters(dependencies, lock)
        ];
        return (module: ApiModule) => {
            for (const func of list) {
                if (!func(module)) {
                    return false;
                }
            }
            return true;
        }
    }


}
