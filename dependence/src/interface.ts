import {ApiComponent} from "./api";

export interface IMeta {
    component: string
    version?: string
    module?: string
    id: string
    dependencies: string[]
}

export enum EnumAudit {
    Config, //указан елемент ли в конфиге   this.config.get(`api.mount.${value}.id`)
    Meta,// this.app.registry.use(element); получение метаданных
    Api
}

export interface OnInit {
    onInit();
}

export interface OnDestroy {
    onDestroy();
}

export type Rule = (api: ApiComponent) => any
