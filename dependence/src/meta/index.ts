import {Api} from "./api";
import {Element} from "./element";

export {Api as MetaApi} from './api'
export {Element as MetaElement} from './element'
export type Meta = Api | Element;
