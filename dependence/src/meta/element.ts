import {satisfies} from "semver";

export class Element {
    static List: Set<string> = new Set([
        'component',
        'module',
        'name',
        'version'
    ]);
    module: string;
    name: string;
    version?: string;

    constructor(value?: object | string) {
        if (value) {
            switch (typeof value) {
                case "object":
                    this.load(value).valid();
                    break;
                case "string":
                    this.id = value;
                    this.valid();
                    break;
                default:
                    throw new Error();
            }

        }
    }

    get component(): string {
        return (this.version) ? `${this.name}@${this.version}` : this.name;
    }

    set component(value: string) {
        [this.name, this.version] = value.split('@', 2);
    }

    get id(): string {
        return `${this.module}://${this.component}`;
    }

    set id(value: string) {
        if (!/^.*:\/\/.*$/gmi.test(value)) {
            throw new Error(`не явялется meta.dependence id: ${value}`)
        }
        [, this.module, this.component] = /^(?<module>.*?):\/\/(?<component>.*?)$/gmi.exec(value);
    }

    static isValid(value: string): boolean {
        return /^.*:\/\/.*$/gmi.test(value);
    }

    isVersion(): boolean {
        return this.version !== undefined;
    }

    change(value: object): this {
        for (const name of Object.keys(value).filter(value1 => Element.List.has(value1))) {
            if (value[name]) {
                this[name] = value[name];
            }
        }
        return this;
    }

    load(value: object): this {
        return this
            .clear()
            .change(value)
    }

    clear(): this {
        Element.List.forEach(value => delete this[value]);
        return this;
    }

    valid(): this {
        for (const name of ['name', 'module']) {
            if (typeof this[name] !== 'string') {
                throw new Error(`${name}`);
            }
        }
        if (!['string', 'undefined'].includes(typeof this.version)) {
            throw new Error();
        }
        return this;
    }

    satisfies(version?: string) {
        if (version === '*' || this.version === version || typeof version === "undefined" || typeof this.version === "undefined") {
            return true
        }
        if (typeof version !== 'string') {
            throw  new Error();
        }
        return satisfies(version, this.version);
    }
}
