import {Element} from "./element";

export class Api {
    mount: string;
    element?: Element;

    constructor(value?: object | string) {
        if (value) {
            switch (typeof value) {
                case "object":
                    this.load(value);
                    break;
                case "string":
                    this.id = value;
                    break;
                default:
                    throw new Error();
            }
            this.valid();
        }
    }

    get id(): string {
        return (this.isElement()) ? `${this.mount}::${this.element.id}` : this.mount;
    }

    set id(value: string) {
        if (/^\/.*::.*:\/\/.*$/gmi.test(value)) {
            if (!this.isElement()) {
                this.element = new Element();
            }
            [this.mount, this.element.id] = value.split('::', 2)
        } else if (/^\/.*$/gmi.test(value)) {
            this.mount = value;
            delete this.element;
        } else {
            throw new Error()
        }

    }

    static isValid(value: string): boolean {
        return (/^\/.*::.*:\/\/.*$/gmi.test(value) || /^\/.*$/gmi.test(value));
    }

    isElement(): boolean {
        return (this.element !== undefined);
    }

    valid(): this {
        if (typeof this.mount !== 'string') {
            throw new Error(`mount`);
        }
        if (this.isElement()) {
            if (!(this.element instanceof Element)) {
                throw new Error();
            }
            this.element.valid();
        }
        return this;
    }

    load(value: any): this {
        return this
            .clear()
            .change(value);
    }

    clear(): this {
        delete this.mount;
        delete this.element;
        return this;
    }

    change(value: any): this {
        if (value.hasOwnProperty('mount')) {
            this.mount = value.mount;
        }
        if (Object.keys(value).filter(value1 => Element.List.has(value1)).length > 0) {
            if (this.isElement()) {
                this.element.change(value)
            } else {
                this.element = new Element(value)
            }
        }
        return this;
    }
}
