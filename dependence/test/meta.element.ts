import {MetaElement} from "../src/meta";
import {expect} from "chai";

describe(`MetaElement`, function () {
    it('без версии', function () {
        const module = 'service';
        const name = 'http-kernel';
        const id = `${module}://${name}`;
        for (const meta of [
            new MetaElement(id),
            new MetaElement({module, name})
        ]) {
            expect(meta.id).eq(id);
            expect(meta.module).eq(module);
            expect(meta.name).eq(name);
            expect(meta.isVersion()).eq(false);
            expect(meta.version).eq(undefined);
        }

    });
    it('версия', function () {
        const module = 'service';
        const name = 'http-kernel';
        const version = '1.2';
        const id = `${module}://${name}@${version}`;
        for (const meta of [
            new MetaElement(id),
            new MetaElement({module, name, version})
        ]) {
            expect(meta.id).eq(id);
            expect(meta.module).eq(module);
            expect(meta.name).eq(name);
            expect(meta.isVersion()).eq(true);
            expect(meta.version).eq(version);
        }
    });
    it('вилидация', function () {
        expect(function () {
            return new MetaElement('service')
        }).to.throw()
        expect(function () {
            // @ts-ignore
            return new MetaElement(5)
        }).to.throw();
        const meta = new MetaElement('service://http-kernel@1.2');
        expect(function () {
            // @ts-ignore
            meta.name = 5;
            meta.valid();
        }).to.throw()
        expect(function () {
            // @ts-ignore
            meta.module = 5;
            meta.valid();
        }).to.throw()

        expect(function () {
            // @ts-ignore
            meta.version = 5;
            meta.valid();
        }).to.throw()

    });
    it('версионость', function () {
        const meta = new MetaElement('service://http-kernel@1.2');
        expect(meta.satisfies()).eq(true);
        expect(meta.satisfies('*')).eq(true);
        // expect(meta.satisfies('<2')).eq(true);
        expect(meta.satisfies('>2')).eq(false);
        expect(function () {
            // @ts-ignore
            meta.satisfies(2)
        }).to.throw()

    });
});
