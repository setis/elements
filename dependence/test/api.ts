import {Kernel} from "../src/kernel";
import {ApiComponent} from "../src/api";
import {MetaApi} from "../src/meta";
import * as chai from "chai";
// import {expect} from "chai";
import {EnumStrategy} from "../src/api/module";
import {Base} from "../src/api/base";
import * as chaiAsPromised from "chai-as-promised";
import {EventEmitter} from "events";

chai.use(chaiAsPromised);
// chai.should();
const expect = chai.expect;
describe(`Api`, function () {

    it('ApiComponent без Module ', function () {
        const kernel = new Kernel();
        const meta = new MetaApi('/http/server');
        const component = new ApiComponent(kernel, meta);
        expect(component.name()).eq('api');
        expect(component.isModule()).eq(false);
        component.state = true;
        // component.emitter.on('state',args => )
        expect(component.state).eq(true);
        const strategy = component.strategy;
        component.strategy = strategy;
        expect(component.strategy).eq(strategy)
        for (const strategy of [EnumStrategy.None, EnumStrategy.Timer, EnumStrategy.Change]) {
            component.strategy = strategy;
            expect(component.strategy).eq(strategy);
        }
        expect(() => {
            // @ts-ignore
            component.strategy = 'ssss';
        }).to.throw();


        expect(() => {
            // @ts-ignore
            const component = new ApiComponent(kernel, new Object());
            component.name();
        }).to.throw();
    });
    it('Base', async function () {
        class Service extends Base {
            status: boolean = false;

            constructor(
                public id: string
            ) {
                super();

            }

            async check() {
                this.state = this.status;
            }

        }

        const service = new Service('s1');
        expect(() => {
            // @ts-ignore
            service.state = 5;
        }).to.throw;
        expect(service.emitter).instanceOf(EventEmitter);
        // service.emitter.on('state',state=>{
        //     console.log(`state: ${state.toString()} time: ${new Date().getTime()}`,new Error().stack)
        // })
        for (const list of [
            ['up', true],
            ['down', false]
        ]) {
            const [method, state] = list as [string, boolean];
            service.state = !state;
            expect(service[method](1)).to.be.rejected;
            service.state = state;
            await service[method](1);
            service.state = !state;
            setTimeout(() => {
                service.state = state;
            }, 3);
            await service[method](10);

        }
    });
    it('ApiModule без Component', function () {

    });
    it('ApiModule + ApiComponent', function () {

    });
})
