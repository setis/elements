import {MetaApi, MetaElement} from "../src/meta";
import {expect} from "chai";

describe(`MetaApi`, function () {
    it('без MetaElement', function () {
        const name = '/http/server';
        const meta = new MetaApi(name);
        expect(meta.mount).eq(name);
        expect(meta.id).eq(name);
        ;
        expect(meta.isElement()).eq(false);
        expect(meta.element).eq(undefined);

    });
    it('c MetaElement', function () {
        const mount = '/http/server';
        const element = 'service://http-kernel@1.2';
        const id = `${mount}::${element}`;
        const meta = new MetaApi(id);
        expect(meta.id).eq(id);
        expect(meta.element).instanceOf(MetaElement);
        expect(meta.isElement()).eq(true);
        expect(meta.element.id).eq(element);
        expect(meta.mount).eq(mount);
        delete meta.element;
        expect(meta.isElement()).eq(false);
        expect(meta.element).eq(undefined);

        const meta2 = new MetaApi({
            mount,
            name: 'http-kernel',
            version: '1.2',
            module: 'service'
        });
        expect(meta2.id).eq(id);
        expect(meta2.element).instanceOf(MetaElement);
        expect(meta2.isElement()).eq(true);
        expect(meta2.element.id).eq(element);
        expect(meta2.mount).eq(mount);

    });
    it('валидация', function () {
        expect(function () {
            return new MetaApi('http/server')
        }).to.throw()
        expect(function () {
            // @ts-ignore
            return new MetaApi(5)
        }).to.throw()
        expect(function () {
            const meta = new MetaApi('/http/server');
            // @ts-ignore
            meta.element = new Object();
            meta.valid();
        }).to.throw()
        expect(function () {
            const meta = new MetaApi('/http/server');
            // @ts-ignore
            meta.mount = 5;
            meta.valid();
        }).to.throw()
        expect(function () {
            const meta = new MetaApi('/http/server::service://http-kernel@1.2');
            meta.change({
                version: () => {
                }
            });
            meta.valid();
        }).to.throw()
    });
});
