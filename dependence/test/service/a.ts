import {EventEmitter} from "events";
import {MetaElement} from "../../src/meta";

export class ServiceA {
    readonly emitter: EventEmitter = new EventEmitter();

    constructor(
        readonly meta: MetaElement,
        state: boolean = false
    ) {
        this.state = state;
    }

    protected _state: boolean = false;

    get state(): boolean {
        return this._state;
    }

    set state(value: boolean) {
        if (typeof value !== 'boolean') {
            throw new Error(`ох , бл...`);
        }
        if (this._state === value) {
            return;
        }
        this._state = value;
        this.emitter.emit('state', value);
    }
}
