import {MetaElement} from "../../src/meta";
import {random} from "lodash";
import {ServiceA} from "./a";
import Timeout = NodeJS.Timeout;

export class ServiceB extends ServiceA {
    id?: Timeout;

    constructor(
        meta: MetaElement
    ) {
        super(meta);
    }

    get timeout(): number {
        return random(random(0, 15), random(random(15, 75)))
    }

    get status(): boolean {
        return (random(0, 5) + random(0, 10)) > 10;
    }

    onTimer() {
        if (!this.id) {
            this.id = setTimeout(() => {
                this.state = this.status;
                delete this.id;
                this.onTimer();
            }, this.timeout).unref();
        }
    }

    offTimer() {
        if (this.id) {
            clearInterval(this.id);
            delete this.id;
        }
    }

    onInit() {
        this.onTimer();
    }

    onDestroy() {
        this.offTimer();
    }
}
