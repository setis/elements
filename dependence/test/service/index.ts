import {ServiceB} from "./b";
import {ServiceA} from "./a";

export type Service = ServiceA | ServiceB;
export * from './a'
export * from './b'
