import {ApiComponent} from "../src/api";
import {Kernel} from "../src/kernel";
import {expect} from "chai";
import {MetaApi, MetaElement} from "../src/meta";
import {Component} from "../src/api/component";
import {Service} from "./service";

describe(`Kernel`, function () {


    const rules: Map<string, any> = new Map([
        [
            'api.install',
            function (api: ApiComponent) {
                api.kernel.metas
            }
        ]
    ]);

    it('meta', function () {
        const kernel = new Kernel();
        const list = ['/http/server', '/http/server::service://http-kernel@1.2'];
        for (const name of list) {
            const meta = kernel.useMeta(name);
            expect(meta).instanceOf(MetaApi);
            expect(meta.id).eq(name);
        }
        expect(kernel.metas.size).eq(list.length);
        const name = 'service://http-kernel@1.2';
        const meta = kernel.useMeta(name);
        expect(meta).instanceOf(MetaElement);
        expect(meta.id).eq(name);
        expect(kernel.metas.size).eq(list.length + 1);
        expect(function () {
            return kernel.useMeta('name')
        }).to.throw();
        expect(kernel.metas.size).eq(list.length + 1);
    });
    it(`component`, async function () {
        const kernel = new Kernel();
        const list = ['/http/server', '/http/server::service://http-kernel@1.2'];
        const dispather: Map<string, Service> = new Map();
        kernel.rules
            .set('api.install', (value: Component) => {
                expect(value).instanceOf(Component);
                expect(value.meta).instanceOf(MetaApi);
            })
            .set('api.uninstall', (value: Component) => {
                expect(value).instanceOf(Component);
                expect(value.meta).instanceOf(MetaApi);
            })
            .set('api', (value: Component) => {
                expect(value).instanceOf(Component);
                expect(value.meta).instanceOf(MetaApi);
            });
        for (const name of list) {
            const component = await kernel.useComponent(name);
            expect(component).instanceOf(Component);
            expect(component.id).eq(name);
            expect(component.name()).eq('api')
        }
    });

})
