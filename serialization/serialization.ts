import {Config} from '../configure';
import {ICode, IConfigure, ILoader, IScheme} from './interface';
import {Meta} from './meta';

export class Serialization {
    readonly format: Map<string, Meta> = new Map();
    config: Config<IConfigure>;
    readonly store: Map<string, ICode<any, any> | IScheme> = new Map();

    async use(name: string): Promise<IScheme | ICode<any, any>> {
        if (this.store.has(name)) {
            return this.store.get(name)
        }
        const configure = this.config.get(`config.${name}`);
        if (!configure) {
            throw new Error(`not found serialization.config.${name}`)
        }
        const {format, loader, src, dst, config} = configure;
        if (!this.format.has(format)) {
            throw new Error(`serialization.config.${name} not load format: ${format} `)
        }
        const meta = this.format.get(format);
        let container;
        switch (loader) {
            case ILoader.Dynamic:
                container = await meta.dynamic(configure);
                break;
            case ILoader.Static:
                container = await meta.static(dst);
                break;
            case ILoader.Compile:
                if (typeof meta.compile !== 'function') {
                    throw new Error(`не доступна функция компилация`)
                }
                await meta.compile(src, dst, config);
                container = await meta.static(dst);
                break;
            default:
                throw new Error();
        }
        this.store.set(name, container);
        return container;
    }
}
