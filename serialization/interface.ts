export interface ICode<E, D> {
    encode(value: D): E | Promise<E>;

    decode(value: E): D | Promise<D>;

}

export enum IType {
    NoScheme,
    Scheme
}

export enum ILoader {
    Dynamic = 'dynamic',
    Compile = 'compile',
    Static = 'static'
}

export interface IStructure {
    name: string;
    type: IType;
    loader_compile?: (...args) => Promise<void>;
    loader_dynamic?: (configure: any) => Promise<IScheme | ICode<any, any>> | IScheme | ICode<any, any>
    loader_static?: (...args) => Promise<IScheme | ICode<any, any>> | IScheme | ICode<any, any>

    [x: string]: any
}

export interface IScheme {
    message<E = any, D = any>(scheme: string): Promise<ICode<E, D>> | ICode<E, D>;

    message(scheme: string): any;

}

export interface IConfigure {
    loader: {
        mode: string[]
        path: string[]
        registry: {
            [format: string]: string
        }
    }
    config: {
        [config: string]: {// для схем
            loader: ILoader | string,// только кто имеет схемы
            format: string
            src?: string[] | string
            dst?: string
            config?: any

        } | {// для без схем
            format: string
            config?: any
        }
    }
}
