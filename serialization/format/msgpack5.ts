import {ICode, IType} from '../interface';

export class SerializationMsgPack5<E = Buffer, D = any> implements ICode<E, D> {
    readonly name: string = name;
    protected container;

    constructor() {
        try {
            this.container = require('msgpack5')();

        } catch (e) {
            throw new Error(`Пакет «msgpack5» отсутствует! Пожалуйста, установите его с помощью команды npm install msgpack5 --save!`);
        }
    }

    decode(value: E): Promise<D> | D {
        return this.container.decode(value);
    }

    encode(value: D): Promise<E> | E {
        return this.container.encode(value);
    }

}

export const name = 'msgpack5';
export const type = IType.NoScheme;

export async function dynamic() {
    return new SerializationMsgPack5();
}
