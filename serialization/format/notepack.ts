import {ICode, IType} from '../interface';

export class SerializationNotePack<E = Buffer, D = any> implements ICode<E, D> {
    readonly name: string = name;
    protected container;

    constructor() {
        try {
            this.container = require('notepack.io');
        } catch (e) {
            throw new Error(`Пакет «notepack.io» отсутствует! Пожалуйста, установите его с помощью команды npm install notepack.io --save!`);
        }
    }

    decode(value: E): Promise<D> | D {
        return this.container.decode(value);
    }

    encode(value: D): Promise<E> | E {
        return this.container.encode(value);
    }

}

export const name = 'notepack.io';
export const type = IType.NoScheme;

export async function dynamic() {
    return new SerializationNotePack();
}
