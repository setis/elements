import {ICode, IType} from '../interface';

export class SerializationAvro<E = string | Buffer, D = any> implements ICode<E, D> {
    protected container;

    constructor() {
        try {
            this.container = require('avsc');
        } catch (e) {
            throw new Error(`Пакет «avsc» отсутствует! Пожалуйста, установите его с помощью команды npm install avsc --save!`)
        }
    }

    decode(value: E): D {
        return this.container.decode(value);
    }


    encode(value: D): Promise<E> | E {
        return this.container.encode(value);
    }

}

export const name = 'avro';
export const type = IType.NoScheme;

export async function dynamic() {
    return new SerializationAvro();
}
