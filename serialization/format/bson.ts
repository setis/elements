import {IType} from '../interface';

export const name = 'bson';
export const type = IType.Scheme;

/**
 * @link https://www.npmjs.com/package/generate-bson-schema
 * @link https://www.npmjs.com/package/stream-bson
 * @link https://www.npmjs.com/package/bson-ext
 */
export class SerializationBson {
}
