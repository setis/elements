import {X2jOptionsOptional} from 'fast-xml-parser';
import {ICode, IType} from '../interface';

export interface SerializerXmlConfigEncode {
    /**
     * String used for tab, defaults to no tabs (compressed)
     */
    indent?: string;
    /**
     * Return the result as a `stream` (default false)
     */
    stream?: boolean;
    /**
     * Add default xml declaration (default false)
     */
    declaration?: boolean | {
        encoding?: string;
        standalone?: string;
    };
}

export interface SerializerXmlConfig {
    decode: X2jOptionsOptional,
    encode: SerializerXmlConfigEncode

}

export class SerializationXml<E, D> implements ICode<E, D> {
    readonly name: string = name;

    constructor(public configure: SerializerXmlConfig) {
        try {
            const xml = require('xml');
            this.encode = function (value: D, options?: SerializerXmlConfigEncode) {
                const data = (value instanceof Buffer) ? value.toString() : value;
                const config = (options) ? options : this.configure.encode;
                return xml(data, config);
            };
        } catch (e) {
            throw new Error(`Пакет «xml» отсутствует! Пожалуйста, установите его с помощью команды npm install xml --save!`);
        }
        try {
            const container = require('fast-xml-parser');
            this.decode = (value: E, options?: X2jOptionsOptional) => {
                const data = (value instanceof Buffer) ? value.toString() : value;
                const config = (options) ? options : this.configure.decode;
                return container.parse(data, config);
            };
        } catch (e) {
            throw new Error(`Пакет «fast-xml-parser» отсутствует! Пожалуйста, установите его с помощью команды npm install fast-xml-parser --save!`);
        }
    }

    decode(value: E, options?: X2jOptionsOptional): Promise<D> | D {
        throw new Error(`нет метода decode для ${this.name}`);
    }

    encode(value: D, options?: SerializerXmlConfigEncode): Promise<E> | E {
        throw new Error(`нет метода encode для ${this.name}`);
    }

}

export const name = 'xml';
export const type = IType.NoScheme;

export async function dynamic(config: SerializerXmlConfig) {
    return new SerializationXml(config);
}
