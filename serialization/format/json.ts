import {ICode, IType} from '../interface';

/**
 * @link https://www.npmjs.com/package/JSONStream
 */
export class SerializationJson<E = string, D = any> implements ICode<E, D> {
    readonly name: string = name;

    decode(value: E): D {
        // @ts-ignore
        return JSON.parse(value);
    }

    encode(value: D): E {
        // @ts-ignore
        return JSON.stringify(value);
    }

}

export const name = 'json';
export const type = IType.NoScheme;

export async function dynamic() {
    return new SerializationJson();
}
