import {IType} from '../../interface';

export const name = 'protobuf';
export const type = IType.Scheme;
export {
    loader_static,
    StaticMessage as SerializationProtoBufStaticMessage,
    StaticScheme as SerializationProtoBufStaticScheme
} from './static';
export {
    loader_dynamic,
    DynamicScheme as SerializationProtoBufDynamicScheme,
    DynamicMessage as SerializationProtoBufDynamicMessage
} from './dynamic';
export * from './compile';
