import {Scheme} from './scheme';

export {Message as StaticMessage} from './message'
export {Scheme as StaticScheme} from './scheme'

export function loader_static(path: string) {
    return new Scheme(path);
}
