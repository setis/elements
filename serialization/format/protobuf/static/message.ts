import {ICode} from '../../../interface';

export class Message implements ICode<Buffer, Buffer> {

    constructor(
        container: any,
        scheme: string
    ) {
        scheme = scheme[0].toUpperCase() + scheme.slice(1);
        for (const name of ['encode', 'decode']) {
            const method = `${name}${scheme}`;
            if (typeof container[method] !== "function") {
                throw new Error(`not found method: ${method} scheme: ${scheme}`)
            }
            this[name] = value => container[method](value);
        }

    }

    decode(value: Buffer): Promise<Buffer> | Buffer {
        throw new Error(`not found method decode`)
    }

    encode(value: Buffer): Promise<Buffer> | Buffer {
        throw new Error(`not found method decode`)
    }

}
