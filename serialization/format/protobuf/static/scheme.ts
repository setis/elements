import {IScheme} from '../../../interface';
import {Message} from './message';

export class Scheme implements IScheme {
    public container: any;
    readonly cache: Map<string, Message> = new Map();

    constructor(readonly path: string) {
        try {
            this.container = require(path);
        } catch (e) {
            throw e;
        }
    }

    message(scheme: string): Message {
        if (this.cache.has(scheme)) {
            return this.cache.get(scheme);
        }
        const container = new Message(this.container, scheme);
        this.cache.set(scheme, container);
        return container;
    }

}
