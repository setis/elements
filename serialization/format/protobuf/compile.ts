import {parse} from "path";

export async function loader_compile(src: string | string[],
                                     dst: string,
                                     options?: {
                                         type: 'ts' | 'js'
                                     }): Promise<void> {
    try {
        require('protobufjs');
    } catch (e) {
        throw new Error(`Пакет «protobufjs» отсутствует! Пожалуйста, установите его с помощью команды npm install protobufjs --save!`)
    }
    const {type = parse(__filename).ext.slice(1)} = options;
    switch (type) {
        case "ts": {
            const ts = require(`protobufjs/cli/pbts`);
            await new Promise((resolve, reject) => {
                ts.main([
                    '-o',
                    `${dst}.js`,
                    `${dst}.d.ts`,
                    ...(src instanceof Array) ? src : [src]
                ], (err) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            });
        }
        case "js": {
            const js = require(`protobufjs/cli/pbjs`);
            await new Promise((resolve, reject) => {
                js.main([
                    '-t', 'static-module',
                    '-w', 'commonjs',
                    '-o', `${dst}.js`,
                    ...(src instanceof Array) ? src : [src]
                ], (err) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            });
        }
    }
}
