import {Scheme} from './scheme';

export {Scheme as DynamicScheme} from './scheme'
export {Message as DynamicMessage} from './message'

export async function loader_dynamic(path: string | string[]) {
    const container = new Scheme();
    await container.load(path);
    return container;
}
