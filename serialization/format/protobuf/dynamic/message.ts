import {Type} from 'protobufjs';
import {ICode} from '../../../interface';

export class Message implements ICode<Buffer, Buffer> {
    transformation: boolean = false;
    verify: boolean = false;

    constructor(protected container: Type,
                options: { transformation?: boolean, verify?: boolean } = {}) {
        const {transformation = false, verify = false} = options;
        this.transformation = transformation;
        this.verify = verify;
    }

    encode(value: any): Buffer {
        const data = (this.transformation) ? this.transform(value) : value;
        if (this.verify) {
            const verity = this.container.verify(data);
            if (verity) {
                throw new Error(verity);
            }
        }
        const message = this.container.create(data);
        // @ts-ignore
        return this.container.encode(message).finish();
    }

    decode(value: Buffer): any {
        return this.container.decode(value);
    }

    protected transform(data: any) {
        const list = Object.keys(data).filter(value => /(_)/g.test(value));
        if (list.length === 0) {
            return data;
        }
        for (let name of list) {
            let [before, after] = name.split('_', 2);
            let newname = before + after[0].toUpperCase() + after.slice(1);
            data[newname] = data[name];
            delete data[name];
        }
        return data;
    }
}
