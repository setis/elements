import {Root} from 'protobufjs';
import {Message} from './message';

export class Scheme {


    public container: Root;
    readonly messages: Map<string, Message> = new Map();

    constructor() {
        try {
            require('protobufjs');
        } catch (e) {
            throw new Error(`Пакет «protobufjs» отсутствует! Пожалуйста, установите его с помощью команды npm install protobufjs --save!`)
        }
    }

    get path() {
        return this.container ? this.container.files : [];
    }

    async load(path: string | string[]) {
        this.messages.clear();
        this.container = await require('protobufjs').load(path);
    }

    async message(name: string, options?: { transformation: boolean, verify: boolean }): Promise<Message> {
        if (this.messages.has(name)) {
            return this.messages.get(name);
        }
        const container = this.container.lookupTypeOrEnum(name);
        const serialization = new Message(container, options);
        this.messages.set(name, serialization);
        return serialization;
    }

}
