import {ICode, IScheme, IStructure, IType} from './interface';

export class Meta implements IStructure {
    compile?: (...args) => Promise<void>;
    dynamic?: (configure: any) => (Promise<IScheme | ICode<any, any>> | IScheme | ICode<any, any>);
    name: string;
    static?: (...args) => (Promise<IScheme | ICode<any, any>> | IScheme | ICode<any, any>);
    type: IType;

    readonly mode: Set<string> = new Set();

    constructor(parameters?: IStructure) {
        if (parameters) {
            for (const k of ['name', 'type']) {
                if (!parameters[k]) {
                    this[k] = parameters[k];
                }
            }
            for (const name in parameters) {
                if (/^loader_(.*?)$/gmi.test(name)) {
                    const [, mode] = /^loader_(.*?)$/gmi.exec(name);
                    this[mode] = parameters[name];
                    this.mode.add(mode);
                }
            }
            this.valid();
        }
    }

    valid() {
        if (![IType.NoScheme, IType.Scheme].includes(this.type)) {
            throw new Error(`not valid type`);
        }
        if (typeof this.name !== 'string') {
            throw new Error(`not valid name`);
        }
        if (this.mode.size === 0) {
            throw new Error(`нет ни одного режима работы серриализации`);
        }
    }

}
