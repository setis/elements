import {RetrySimple} from './simple';

export * from './intricate';
export * from './simple';
export type RetryCheck = (this: RetrySimple) => boolean;
export type RetryCall = () => Promise<any>;

export interface ParametersRetry {
    counter?: number;
    count?: number;
    timeout?: number;

}

export const defaults: ParametersRetry = {
    counter: 0,
    count: 5,
    timeout: 1e3
};
export const logger = require('debug')('element:retry');
export const log = {
    error: logger.extend('error'),
    warn: logger.extend('warn'),
    info: logger.extend('info'),
    log: logger.extend('log'),
    debug: logger.extend('debug'),
};
