import {defaults, log, ParametersRetry, RetryCall} from './index';
import Timeout = NodeJS.Timeout;

export class RetryIntricate {
    /**
     * @description счетчик попыток
     */
    counter: number;
    /**
     * @description количество попыток
     */
    count: number;
    /**
     * @description интервал между попытками
     */
    timeout: number;
    id: Timeout;
    error?: Error;

    call: string;
    readonly store: Map<string, RetryCall> = new Map();

    constructor(parameters?: ParametersRetry) {
        const options = (parameters) ? {
            ...defaults,
            ...parameters
        } : {...defaults};
        for (const name of ['count', 'counter', 'timeout']) {
            if (options.hasOwnProperty(name) && typeof options[name] === 'number') {
                this[name] = options[name];
            }
        }
    }

    check(call: string): boolean {
        return this.call !== call;
    }

    has(call: string): boolean {
        return this.store.has(call);
    }

    label(name: string): RetryCall {
        if (!this.store.has(name)) {
            throw new Error(`not found call: ${name}`);
        }
        return this.store.get(name);
    }

    async queue(call: string = this.call): Promise<void> {
        if (!this.has(call)) {
            throw new Error(`not found call: ${call}`);
        }
        if (this.check(call)) {
            this.reset();
            this.call = call;
        }
        try {
            const func = this.label(call);
            log.info('intricate', `call: ${call} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
            await func();
            return;
        } catch (e) {
            if (this.count !== 0 && this.counter <= this.count) {
                log.error('intricate', `error: ${e.message} call: ${call}  count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
                throw e;
            }
            this.error = e;
            this.id = setTimeout(this.run.bind(this, call), this.timeout);
            this.counter++;
            log.debug('intricate', `call: ${call} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
        }
    }

    async run(call: string = this.call): Promise<any> {
        if (!this.has(call)) {
            throw new Error(`not found call: ${call}`);
        }
        if (this.check(call)) {
            this.reset();
            this.call = call;
        }
        try {
            const func = this.label(call);
            log.info('intricate', `call: ${call} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
            return await func();
        } catch (e) {
            if (this.count !== 0 && this.counter >= this.count) {
                log.error('intricate', `error: ${e.message} call: ${call}  count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
                throw e;
            }
            this.error = e;
            this.counter++;
            log.debug('intricate', `call: ${call} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
            await new Promise(resolve => {
                this.id = setTimeout(() => {
                    delete this.id;
                    resolve();
                }, this.timeout);
            });
            return this.run(call);
        }
    }

    reset() {
        this.counter = 0;
        this.error = undefined;
        if (this.id) {
            clearTimeout(this.id);
            this.id = undefined;
        }
    }
}
