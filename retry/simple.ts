import Timeout = NodeJS.Timeout;
import {defaults, log, ParametersRetry, RetryCall, RetryCheck} from './index';

export interface RetrySimpleParameters extends ParametersRetry {
    call?: RetryCall;
    check?: RetryCheck;
    message?: string;
}

export class RetrySimple {
    message?: string;
    /**
     * @description счетчик попыток
     */
    counter: number;
    /**
     * @description количество попыток
     */
    count: number;
    /**
     * @description интервал между попытками
     */
    timeout: number;
    id: Timeout;
    error?: Error;

    call: RetryCall;

    constructor(parameters?: RetrySimpleParameters) {
        const options: RetrySimpleParameters = (parameters) ? {
            ...defaults,
            ...parameters
        } : defaults;
        for (const name of ['count', 'counter', 'timeout']) {
            if (options.hasOwnProperty(name) && typeof options[name] === 'number') {
                this[name] = options[name];
            }
        }
        for (const name of ['call', 'check']) {
            if (options.hasOwnProperty(name) && typeof options[name] === 'function') {
                this[name] = options[name];
            }
        }
        if (typeof options.message === 'string') {
            this.message = options.message;
        }
    }

    check(call: RetryCall): boolean {
        return this.call !== call;
    }

    async queue(call: RetryCall = this.call): Promise<void> {
        if (this.check(call)) {
            this.call = call;
            this.reset();
        }
        try {
            log.info('simple', `${this.message} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
            await this.call();
            return;
        } catch (e) {
            if (this.count > 0 || this.counter <= this.count) {
                log.error('simple', `${this.message} error: ${e.message} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
                throw e;
            }
            this.error = e;
            this.id = setTimeout(this.run.bind(this), this.timeout);
            this.counter++;
        }
    }

    async run(call: RetryCall = this.call): Promise<any> {
        if (this.check(call)) {
            this.call = call;
            this.reset();
        }
        try {
            log.info('simple', `${this.message} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
            return await this.call();
        } catch (e) {
            if (this.count > 0 && this.counter >= this.count) {
                log.error('simple', `${this.message} error: ${e.message} count: ${this.count} counter: ${this.counter} timeout: ${this.timeout} `);
                throw e;
            }
            this.error = e;
            this.counter++;
            await new Promise(resolve => {
                this.id = setTimeout(() => {
                    delete this.id;
                    resolve();
                }, this.timeout);
            });
            return this.run();
        }
    }

    reset() {
        this.counter = 0;
        this.error = undefined;
        if (this.id) {
            clearTimeout(this.id);
            this.id = undefined;
        }
    }
}
