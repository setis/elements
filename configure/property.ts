import {Observable} from 'object-observer/dist/node/object-observer.js';
import {Subject, Subscription} from 'rxjs';
import {filter, map} from 'rxjs/operators';

function getPath(obj: Object, path: string, separator: string = '.'): { error: boolean, path: string, value: any } {
    const pathArr = path.split(separator);
    const parts = [];
    while (pathArr.length !== 0) {
        const name = pathArr.shift();
        if (obj instanceof Object && obj.hasOwnProperty(name)) {
            parts.push(name);
            obj = obj[name];
        } else {
            return {error: true, path: parts.join(separator), value: obj};
        }
    }
    return {error: false, path: parts.join('.'), value: obj};
}

function setPath(obj: Object, path: string, value: any, separator: string = '.') {
    const pathArr = path.split(separator);
    const parts = [];
    while (pathArr.length !== 0) {
        const name = pathArr.shift();
        if (obj instanceof Object && obj.hasOwnProperty(name)) {
            parts.push(name);
            if (pathArr.length === 0) {
                obj[name] = value;
            } else {
                obj = obj[name];
            }

        } else {
            if (pathArr.length === 0) {
                obj[name] = value;
            } else {
                obj[name] = {};
                obj = obj[name];
            }
        }
    }
}

function removePath(obj: Object, path: string, separator: string = '.') {
    const pathArr = path.split(separator);
    const parts = [];
    while (pathArr.length > 0) {
        const name = pathArr.shift();
        if (obj instanceof Object && obj.hasOwnProperty(name)) {
            parts.push(name);
            if (pathArr.length === 0) {
                delete obj[name];
                return;
            } else {
                obj = obj[name];
            }

        } else {
            if (pathArr.length === 0) {
                delete obj[name];
            } else {
                return;
            }
        }
    }
}

function selectCheck(src: string[], dst: string[]): boolean {
    for (const i of src.keys()) {
        if (src[i] !== dst[i]) {
            return (i > 0) ? true : false;
        }
    }
    return true;
}

function selectSlice(src: string[], dst: string[]): string[] {
    for (const i of src.keys()) {
        if (src[i] !== dst[i]) {
            if (i > 0) {
                return dst.slice(i - 1);
            }
            throw new Error(`ох , бл...`);
        }
    }
    return dst.slice(src.length - 1);
}

export interface Change<T> {
    type: string | 'update' | 'insert' | 'delete' | 'reverse' | 'shuffle';
    oldValue?: any;
    value?: any;
    path: string[];
    object: T;
    link: string;
}

export interface IConfig {
    get(path?: string, value?: any): any;

    set(path: string, value: any);

    delete(path: string);

    has(path: string): boolean;
}

const logger = require('debug')('element:config:property');
const log = {
    error: logger.extend('error'),
    info: logger.extend('info'),
    log: logger.extend('log'),
};

export class Property<T = any> implements IConfig {
    readonly subject: Subject<Change<T>> = new Subject();
    readonly subscription: Subscription = new Subscription();
    protected store: Map<string, Property> = new Map();

    constructor(
        readonly separator: string = '.',
        readonly parent?: { store: Property; path: string }) {

    }

    protected _data: T;

    get data(): T {
        if (this.parent && !this._data) {
            const {store, path} = this.parent;
            this._data = store.get(path, {});
        }
        return this._data;

    }

    set data(value: T) {
        if (this.parent) {
            const {store, path} = this.parent;
            store.set(path, value);
            const pathForm = path.split(this.separator);
            this.subscription.unsubscribe();
            this.subscription.add(
                store.subject
                    .pipe(
                        filter(value1 => selectCheck(pathForm, value1.path)),
                        map(value1 => {
                            value1.path = selectSlice(value1.path, pathForm);
                            value1.link = value1.path.join(this.separator);
                            return value1;
                        })
                    ).subscribe(this.subject)
            );
            this._data = value;
        } else {
            const data = Observable.from(value);
            data.observe(changes => {
                changes.forEach(change => {
                    this.subject.next({
                        ...change,
                        link: change.path.join(this.separator)
                    });
                });
            });
            this._data = data;
        }

    }

    get absolute(): string {
        return (this.parent) ? this.parent.path : '';
    }

    get root() {
        return (this.parent) ? this.parent.store.root : this;
    }

    static Load(value: object, options?: {
        separator?: string
        parent?: { store: Property; path: string }
    }) {
        const container = new Property(options.separator, options.parent);
        container.data = value;
        return container;
    }

    async onDestroy() {
        if (this.parent) {
            const {store, path} = this.parent;
            store.delete(path);
        }
        this.subscription.unsubscribe();
    }

    get<T = any>(): T;

    get<T = any>(path: string, value?: any): T;

    get(path: null | undefined | '', value?: any): T;

    get(path?: string, value?: any) {
        if (path === undefined || path === '' || !path || path === null) {
            return this.data;
        }
        const result = getPath(this.data, path, this.separator);
        if (result.error) {
            if (value !== undefined) {
                return value;
            }
            const error = new Error(`action: get path: ${path} full: ${this.full(path)} found: ${result.path}`);
            log.error(error.message);
            throw error;
        }
        log.log(`action: get path: ${path} full: ${this.full(path)}`, JSON.stringify(result.value));
        return result.value;
    }

    set(path: string, value: any): this {
        setPath(this.data, path, value, this.separator);
        log.log(`action: set path: ${path} full: ${this.full(path)}`, JSON.stringify(value));
        return this;
    }

    has(path: string): boolean {
        return !getPath(this.data, path).error;
    }

    delete(path: string): this {
        removePath(this.data, path, this.separator);
        log.log(`action: delete path: ${path} full: ${this.full(path)}`);
        return this;
    }

    full(name?: string): string {
        const result = [];
        if (name) {
            result.push(name);
        }
        if (this.parent) {
            result.push(this.parent.path);
            result.push(this.parent.store.full());
        }
        return result
            .filter(value => value !== '')
            .reverse()
            .join('.');
    }

    use<C = T>(path: string, notFoundError: boolean = false): Property<C> {
        if (!this.has(path) && notFoundError) {
            throw new Error(`нет такого ${path}`);
        }
        const id = (this.parent) ? this.full(path) : path;
        const store = this.root.store;
        if (store.has(id)) {
            return store.get(id);
        }
        const property = new Property(this.separator, {
            store: this.root,
            path: id,
        });
        store.set(id, property);
        log.log(`action: use path: ${path} full: ${this.full(path)}`);
        return property;
    }
}
