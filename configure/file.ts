import {access, constants, exists, readFile} from 'fs';
import {dirname, join, normalize, parse} from 'path';
import {Subject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {promisify} from 'util';
import {iFormat} from './format';
import FormatYaml from './format/yaml';
import parseVar from './parse';
import {Property} from './property';

const fg = require('fast-glob');
import deepmerge = require('deepmerge');

const {R_OK} = constants;
const fs_exists = promisify(exists);
const fs_access = promisify(access);

export interface SettingFile {
    separator: string;
    path: string;
    pattern: string[];
    params?: Object;
    timeout?: number;
    env?: boolean;
}

export interface Import {
    data: string;
    import: (path: string) => Promise<Import>;
    path: string;
}

const logger = require('debug')('element:config:file');
const log = {
    error: logger.extend('error'),
    info: logger.extend('info'),
    log: logger.extend('log'),
};

export class File<T = any> extends Property<T> {
    params: Object = {};
    format: Set<iFormat> = new Set();
    ext: Set<string> = new Set();
    path: string;
    pattern: Set<string> = new Set();
    encoding: string = 'utf8';
    time: Date;
    timeout: number;
    readonly subject_load: Subject<boolean> = new Subject();

    // chunk: Map<string, any> = new Map();
    constructor(config?: SettingFile) {
        super();
        this.load(config);
    }

    private _lock: boolean = false;

    get lock(): boolean {
        return this._lock;
    }

    set lock(value: boolean) {
        this._lock = value;
        if (!value) {
            this.time = new Date();
        } else {
            delete this.time;
        }
        this.subject_load.next(value);
    }

    async file(path: string): Promise<Import> {

        // Get absolute file path
        const absoluteFilePath: string = normalize(path);
        log.info(`action: file path: ${absoluteFilePath} `);
        // Check read access
        let readFlag = R_OK;
        if (!readFlag && constants) {
            readFlag = R_OK;
        }
        try {
            await promisify(access)(absoluteFilePath, readFlag);
        } catch (error) {
            // throw new Error(`Unable to read file: ${absoluteFilePath}`);
            log.error(`Unable to read file: ${absoluteFilePath}`);
            return {
                data: '',
                import: (path1: string) => this.file(join(dirname(path), path1)),
                path
            };
        }
        const content = await promisify(readFile)(path, this.encoding);
        // this.chunk.set(absoluteFilePath,content);
        const relativeDirectory: string = dirname(absoluteFilePath);
        return {
            data: content,
            import: (path: string) => this.file(join(relativeDirectory, path)),
            path
        };
    }

    async run(force: boolean = false) {
        log.info('action: run');
        const loader = async () => {
            delete this.time;
            try {
                await this._run();
            } catch (e) {
                this.subject_load.error(e);
                this.subject_load.next(false);
            }
        };
        if (this.lock) {
            return this.subject_load.pipe(filter(value => value === true)).toPromise();
        }
        if (!this.time) {
            return loader();
        }
        const diff = (new Date().getTime()) - this.time.getTime();
        if (diff <= this.timeout) {
            return loader();
        }
    }

    async _run() {
        this.lock = true;
        const extensions = Array.from(this.ext.values());
        const params = {
            ...this.params,
            ext: extensions
        };
        const list = Array
            .from(this.pattern.values())
            .map(pattern => {
                return pattern.replace(/(\{(.*?)\})/ig, (str, p1, p2, offset, s) => {
                    return (!params.hasOwnProperty(p2)) ? p1 : (params[p2] instanceof Array) ? `{${params[p2].join(',')}}` : params[p2];
                });
            });

        const loaded = await fg(list, {
            cwd: this.path
        });
        let configure = {};
        await Promise.all(loaded.map(async path => {
            const data = await this.file(join(this.path, path));
            const ext = parse(path).ext.slice(1);
            for (const module of this.format) {
                if (module.ext.has(ext)) {
                    const result = await module.require(data.data, data.import, path);
                    configure = deepmerge(configure, result);
                    return result;
                }
            }
        }));
        this.data = parseVar<T>(configure as any);
        this.lock = false;
    }

    /**
     * @todo проблема перезаписью переменных шаблон(pattern) работает только с одним значением, а должен с нескольками
     */
    protected load(config?: SettingFile) {
        this.format.add(new FormatYaml());
        this.format.forEach(value => value.ext.forEach(value => this.ext.add(value)));

        const {
            path = join(__dirname, '../../'),
            params = {},
            pattern = [
                'configure.{ext}',
                'configure-{env}.{ext}',
            ],
            timeout = 1500,
            env = false
        } = config;
        if (env) {
            for (const name in process.env) {
                this.params[name.toLowerCase()] = process.env[name];
            }
        }
        this.path = path;
        this.timeout = timeout;
        pattern.forEach(value => this.pattern.add(value));
        this.params = {...this.params, ...params};
    }

}
