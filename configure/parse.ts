const _ = require('lodash');

function substitute(file, p) {
    let success = false;
    const replaced = p.replace(/\${([\w.-]+)}/g, function (match, term) {
        if (!success) {
            success = _.has(file, term);
        }
        return _.get(file, term) || match;
    });
    return {success, replace: replaced};
}

function transform(file, obj) {
    let changed = false;
    const resultant = _.mapValues(obj, function (p) {
        if (_.isPlainObject(p)) {
            const transformed = transform(file, p);
            if (!changed && transformed.changed) {
                changed = true;
            }
            return transformed.result;
        }
        if (_.isString(p)) {
            const subbed = substitute(file, p);
            if (!changed && subbed.success) {
                changed = true;
            }
            return subbed.replace;
        }
        if (_.isArray(p)) {
            for (let i = 0; i < p.length; i++) {
                if (_.isString(p[i])) {
                    p[i] = substitute(file, p[i]).replace;
                }
            }
        }
        return p;
    });
    return {changed, result: resultant};
}

function readAndSwap<T>(obj: T): T {

    let altered = false;
    do {
        const temp = transform(obj, obj);
        obj = temp.result;
        altered = temp.changed;
    } while (altered);
    return obj;
}

export default readAndSwap;
