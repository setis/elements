export * from './yaml';

export interface iFormat {
    ext: Set<string>;

    require(data: string, loader: (path: string) => Promise<any>, path: string): Promise<any>;
}
