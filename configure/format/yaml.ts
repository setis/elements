import * as deepmerge from 'deepmerge';
import {access, constants, exists} from 'fs';
import {safeLoad} from 'js-yaml';
import {promisify} from 'util';

const {R_OK} = constants;
const fs_exists = promisify(exists);
const fs_access = promisify(access);
const logger = require('debug')('element:config:format:yaml');
const log = {
    error: logger.extend('error'),
    info: logger.extend('info'),
    log: logger.extend('log'),
};
export default class FormatYaml {
    readonly ext: Set<string> = new Set(['yaml', 'yml']);

    async require(data: string, loader: (path: string) => Promise<any>, path: string): Promise<any> {

        // Convert YAML content to object
        let config: any;
        try {
            config = safeLoad(data);
        } catch (e) {
            log.error(`path: ${path} msg:${e.message} text:${data}`);
            throw  new Error(`path: ${path} msg:${e.message} text:${data}`);
        }
        if (typeof config !== 'object') {
            config = {};
        }

        // Handle "imports" directive
        if (config.hasOwnProperty('imports') && Array.isArray(config.imports)) {

            // Build a base configuration
            let baseConfig = {};
            for (const importEntry of config.imports) {
                if (!importEntry.hasOwnProperty('resource')) {
                    continue;
                }

                const loaded = await loader(`${importEntry.resource}`);
                log.info(`path: ${path} import resource: ${importEntry.resource} property:${importEntry.property}`);
                // Entry configuration
                const entryConfig = await this.require(loaded.data, loaded.import, loaded.import);

                // By default, the merge is done on the configuration root
                let targetProperty = null;
                let property = baseConfig;
                if (importEntry.hasOwnProperty('property')) {
                    targetProperty = importEntry.property;
                }
                if (targetProperty) {
                    const propertyPath = targetProperty.split('.');
                    let parentProperty = property;
                    let lastPropertyName = null;
                    for (const propertyName of propertyPath) {
                        lastPropertyName = propertyName;
                        parentProperty = property;

                        if (typeof property === 'object' && property.hasOwnProperty(propertyName)) {
                            property = property[propertyName];
                        } else {
                            // Create the property if it does not exist
                            property = property[propertyName] = {};
                        }
                    }
                    if (lastPropertyName !== null) {
                        parentProperty[lastPropertyName] = deepmerge(property, entryConfig);
                    }
                } else {
                    baseConfig = deepmerge(baseConfig, entryConfig);
                }
            }
            // Override the base configuration with the current configuration
            config = deepmerge(config, baseConfig);

            // Remove import entries from configuration
            delete config.imports;
        }

        return config;
    }

}
