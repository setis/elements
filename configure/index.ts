import {File} from './file';
import {Property} from './property';

export * from './parse';
export * from './property';
export * from './file';
export * from './format';
export * from './boot';
export type Config<T = any> = File<T> | Property<T>;
