import {File, SettingFile} from './file';
import {Property} from './property';

export interface ParametesBootStrap<Data = any> {
    type: 'data' | 'file' | 'parent' | 'cfg';
    data?: Data;
    file?: SettingFile;
    parent?: {
        store: Property<Data>,
        path: string
    };
    cfg?: File<Data> | Property<Data>;
}

export enum Type {
    Parent,
    File,
    Data
}

export async function bootstrap<T = any>(parameters: ParametesBootStrap<T>) {
    const {type, data, parent, cfg, file} = parameters;
    let config;
    switch (type) {
        case 'data':
            config = new Property();
            config.data = data;
            break;
        case 'file':
            config = new File(file);
            await config.run();
            break;
        case 'parent':
            if (typeof parent.path !== 'string' || !(parent.store instanceof Property)) {
                throw new Error();
            }
            const {store, path} = parent;
            config = store.use(path);
            break;
        case 'cfg':
            if (!(cfg instanceof Property)) {
                throw new Error();
            }
            config = cfg;
            break;
        default:
            throw new Error();
    }
    return config;
}

export function getType<T extends Property = any>(cfg: T) {
    if (!(cfg instanceof Property)) {
        throw new Error();
    }
    if (cfg instanceof File) {
        return Type.File;
    }
    if (cfg.parent) {
        return Type.Parent;
    }
    return Type.Data;
}
