export interface Api<K = string, V = any> {

    has(id: K): boolean | Promise<boolean>;

    set(id: K, data: V, ttl: number | Date): void | Promise<void>;

    get(id: K): V | Promise<V>;

    delete(id: K): void | Promise<void>;

    flush(): void | Promise<void>;
}

export interface Adapter<K = string, V = any> extends Api<K, V> {
    name: string;

    [x: string]: any;
}

export interface OnInit {
    onInit(): void | Promise<void>;
}

export interface OnDestroy {
    onDestroy(): void | Promise<void>;
}


export interface Config extends Setting {
    adapter: string;
    config: any;

}

export interface Code<E = string, D = any> {
    encode: (value: D) => E | Promise<E>;
    decode: (value: E) => D | Promise<D>;
}

export interface Setting {
    prefix?: (value: any) => any;
    ttl?: number;
}

export interface ManagerConfig {
    [name: string]: Config
}
