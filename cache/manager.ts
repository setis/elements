import {Redis, RedisOptions} from 'ioredis';
import {AdapterMemory, AdapterRedis} from './adapters';
import {Cache} from './cache';
import {Adapter, Config, ManagerConfig} from './interface';

export class Manager {
    readonly adapters: Map<string, (config?: any) => Adapter> = new Map();
    readonly store: Map<string, Cache> = new Map();

    constructor(protected configure: ManagerConfig) {
    }

    async create(config: Config) {
        if (!this.adapters.has(config.adapter)) {
            throw new Error(`не найден адаптер: ${config.adapter}`);
        }
        const adapter = this.adapters.get(config.adapter)(config.config);
        const cache = new Cache(config);
        cache.adapter = adapter;
        await cache.onInit();
        return cache;
    }

    async remove(name: string)
    async remove(cache: Cache)
    async remove(NameOrCache: string | Cache) {
        switch (typeof NameOrCache) {
            case "string":
                const name = NameOrCache as string;
                if (this.store.has(name)) {
                    await this.store.get(name).onDestroy();
                    this.store.delete(name);
                }
                break;
            case "object":
                const cache = NameOrCache as Cache;
                //@ts-ignore
                if (data instanceof Cache || data.getClass().implements.indexOf('CacheHandler') === -1) {
                    throw new Error(`аргумент не является Cache или CacheHandler`)
                }
                for (let [name, adapter] of this.store) {
                    if (adapter == cache) {
                        await cache.onDestroy();
                        this.store.delete(name);
                        return;
                    }
                }
                break;
            default:
                throw new Error();
        }
    }


    async use(name: string): Promise<Cache>;
    async use(config: Config): Promise<Cache>;
    async use(NameOrConfig: string | Config): Promise<Cache> {
        switch (typeof NameOrConfig) {
            case "string": {
                const name = NameOrConfig as string;
                let container: Cache;
                if (!this.configure.hasOwnProperty(name)) {
                    throw new Error(`нет такого конфига:${name} для cache.manager`);
                }
                const profile = JSON.stringify(this.configure[name]);
                if (this.store.has(profile)) {
                    return this.store.get(profile);
                }
                container = await this.create(this.configure[name]);
                this.store.set(profile, container);
                return container;
            }

            case "object": {
                const config = NameOrConfig as Config;
                const profile = JSON.stringify(config) as string;
                if (this.store.has(profile)) {
                    return this.store.get(profile);
                }
                const container = await this.create(config);
                this.store.set(profile, container);
                return container;
            }
            default:
                throw new Error();
        }

    }

    onInit() {
        this.adapters
            .set('redis', (config: RedisOptions | Redis) => new AdapterRedis(config))
            .set('memory', () => new AdapterMemory())
    }

    async onDestroy() {
        let arr = [];
        for (let [name, adapter] of this.store) {
            arr.push(async () => {
                try {
                    await adapter.onDestroy();
                } catch (e) {
                    throw new Error(`adapter:${name} ${e.message}`)
                }
            });
        }
        await Promise.all(arr);
    }
}
