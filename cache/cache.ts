import {Adapter, Api, Code, Setting} from './interface';

export class Cache<K = string | Buffer, V = any> implements Api<K, V> {
    public ttl?: number;
    public prefix?: (value: any) => any;

    constructor(config: Setting) {
        const {ttl, prefix} = config;
        if (ttl) {
            this.ttl = ttl;
        }
        if (prefix) {
            this.prefix = prefix;
        }
    }

    private _adapter: Adapter;

    get adapter(): Adapter {
        if (!this._adapter) {
            throw new Error(`не установлен adapter`);
        }
        return this._adapter;
    }

    set adapter(value: Adapter) {
        this._adapter = value;
    }

    private _format: Code;

    get format() {
        return this._format;
    }

    set format(value: Code) {
        if (typeof value.encode !== 'function') {
            throw  new Error(`не найден функция encode`);
        }
        if (typeof value.decode !== 'function') {
            throw  new Error(`не найден функция decode`);
        }
        this._format = value;
    }

    delete(id: K): void | Promise<void> {

        return this.adapter.delete(this.prefix ? this.prefix(id) : id);
    }

    get(id: K): Promise<V> | V {
        return this.adapter.get(this.prefix ? this.prefix(id) : id);
    }

    has(id: K): boolean | Promise<boolean> {
        return this.adapter.has(this.prefix ? this.prefix(id) : id);
    }

    set(id: K, data: V, ttl: number | Date = this.ttl): void | Promise<void> {
        return this.adapter.set(this.prefix ? this.prefix(id) : id, (this.format) ? this.format.encode(data) : data, ttl);
    }

    flush(): void | Promise<void> {
        return this.adapter.flush();
    }

    async onInit() {
        if (typeof this.adapter.onInit === 'function') {
            await this.adapter.onInit();
        }

    }

    async onDestroy() {
        if (typeof this.adapter.onDestroy === 'function') {
            await this.adapter.onDestroy();
        }
    }

}
