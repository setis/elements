import {Adapter} from '../interface';

export class AdapterMemory<K = string, V = any> implements Adapter<K, V> {
    readonly name: string = 'memory';
    protected store: Map<K, {
        data: V,
        date: Date,
        expire?: Date,
        timeout?: NodeJS.Timeout
    }> = new Map();

    has(id: K): boolean {
        return this.store.has(id);
    }

    set(id: K, value: V, ttl?: number | Date) {
        const date = new Date();
        const data = {
            data: value,
            date,
            ...(ttl) ? this.setTimeout(id, ttl, date) : {}
        }
        this.clearTimeout(id);
        // @ts-ignore
        this.store.set(id, data);
    }

    get(id: K) {
        const data = this.store.get(id);
        if (data) {
            return data.data;
        }
    }

    delete(...args: K[]) {
        for (const id of args) {
            this.clearTimeout(id);
            this.store.delete(id);
        }

    }

    flush() {
        for (const [key, value] of this.store) {
            const {timeout} = value;
            if (timeout) {
                clearTimeout(timeout);
            }
            this.delete(key);
        }
    }

    protected handler(id: K) {
        this.store.delete(id);
    }

    protected setTimeout(id: K, value: number | Date, date: Date = new Date()) {

        switch (typeof value) {
            case "number": {
                return {
                    timeout: setTimeout(this.handler.bind(this, id), value),
                    expire: new Date().setTime(date.getTime() + value)
                }
            }
            case "object": {
                if (!(value instanceof Date)) {
                    throw new Error();
                }
                const timeout = value.getTime() - date.getTime();
                if (timeout < 1) {
                    throw new Error(`меньше 1 ms живет`)
                }
                return {
                    timeout: setTimeout(this.handler.bind(this, id), timeout),
                    expire: value
                }
            }
            default:
                throw new Error();
        }
    }

    protected clearTimeout(id: K) {
        if (this.store.has(id)) {
            const {timeout} = this.store.get(id);
            if (timeout) {
                clearTimeout(timeout);
            }
        }
        ;
    }

}
