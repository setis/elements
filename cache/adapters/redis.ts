import {EventEmitter} from 'events';
import * as RedisStorage from 'ioredis';
import {Redis, RedisOptions} from 'ioredis';
import {Adapter} from '../interface';

export class AdapterRedis<K = string | Buffer, V = string> implements Adapter<K, V> {
    readonly name: string = 'redis';
    protected store: Redis;
    protected initialized: boolean = false;

    constructor(config: RedisOptions | Redis) {
        if (config instanceof EventEmitter) {
            this.store = <Redis>config;
        } else {
            this.store = new RedisStorage(<RedisOptions>config);
        }
    }

    async flush() {
        await this.store.flushdb();
    }

    async onInit() {
        if (this.initialized) {
            return;
        }
        switch (this.store.status) {
            case 'connecting':
            case 'connect':
            case 'ready':
                break;
            default:
                await this.store.connect();
        }

        this.initialized = true;
    }

    async onDestroy() {
        await this.store.disconnect();
        this.initialized = false;
    }

    async has(id: K): Promise<boolean> {
        // @ts-ignore
        const has = await this.store.exists(id);
        return (has === 1);
    }

    async set(id: K, data: V, ttl?: number | Date) {
        let args: any = [id, data];
        if (ttl) {
            switch (typeof ttl) {
                case 'number':
                    args.push('PX', ttl);
                    break;
                case 'object':
                    if (!(ttl instanceof Date)) {
                        throw new Error();
                    }
                    args.push('PX', new Date().getTime() - ttl.getTime());

            }
        }
        return this.store.set.apply(this.store, args);
    }

    get(id: K): Promise<V> {
        // @ts-ignore
        return this.store.get(id);
    }

    async delete(...args: K[]) {
        await Promise.all(
            args.map(this.store.del.bind(this.store))
        )
    }

}
