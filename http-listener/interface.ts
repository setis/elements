import {IncomingMessage, ServerResponse} from 'http';
import {Http2ServerRequest, Http2ServerResponse, SecureServerOptions} from 'http2';
import {ServerOptions} from 'https';
import {ListenOptions} from 'net';

export type Listen =
    { port?: number, hostname?: string, backlog?: number, listeningListener?: Function } |
    { port?: number, hostname?: string, listeningListener?: Function } |
    { port?: number, backlog?: number, listeningListener?: Function } |
    { port?: number, listeningListener?: Function } |
    { path: string, backlog?: number, listeningListener?: Function } |
    { path: string, listeningListener?: Function } |
    { options: ListenOptions, listeningListener?: Function } |
    { handle: any, backlog?: number, listeningListener?: Function } |
    { handle: any, listeningListener?: Function };

export interface Configure {
    protocol: '1' | '2' | 1 | 2;
    safe: SecureServerOptions | ServerOptions | false;
    port: number;
    hostname: string;
}

export interface Handler {
    (req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse): void;
}
