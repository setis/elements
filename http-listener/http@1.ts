import {createServer as HttpCreateServer, Server} from 'http';
import {createServer as HttpsCreateServer, Server as ServerHttp, ServerOptions} from 'https';
import {Handler, Listen} from './interface';

export function nosafe(listen: Listen, request?: Handler, emit?: (server: Server) => void): Promise<Server> {
    const server = HttpCreateServer(request);
    if (typeof emit === 'function') {
        emit(server);
    }
    return new Promise((resolve) => {
        const {port, path, options, handle, listeningListener, hostname, backlog} = (listen as any);
        let func = function () {
            resolve(server);
            // console.log(`http://${hostname}:${port}?version=1.1`);
        };
        if (typeof listeningListener === 'function') {
            func = function () {
                listeningListener.apply(this, arguments);
                resolve(server);
                // console.log(`http://${hostname}:${port}?version=1.1`);
            };
        }
        let args;
        if (port && hostname) {
            args = [port, hostname, func];
        }
        if (port && hostname && backlog) {
            args = [port, hostname, backlog, func];
        }
        if (path && backlog) {
            args = [path, backlog, func];
        }
        if (options && backlog) {
            args = [options, backlog, func];
        }
        if (handle && backlog) {
            args = [handle, backlog, func];
        }
        server.listen.apply(server, args);
    });
}

export function safe(cfg: ServerOptions, listen: Listen, request?: Handler, emit?: (server: ServerHttp) => void): Promise<ServerHttp> {
    const server = HttpsCreateServer(cfg, request);
    if (typeof emit === 'function') {
        emit(server);
    }
    return new Promise((resolve) => {
        const {port, path, options, handle, listeningListener, hostname, backlog} = (listen as any);
        let func = function () {
            resolve(server);
        };
        if (typeof listeningListener === 'function') {
            func = function () {
                listeningListener.apply(this, arguments);
                resolve(server);
                console.log(`https://${hostname}:${port}?version=1.1`);
            };
        }
        let args;
        if (port && hostname) {
            args = [port, hostname, func];
        }
        if (port && hostname && backlog) {
            args = [port, hostname, backlog, func];
        }
        if (path && backlog) {
            args = [path, backlog, func];
        }
        if (options && backlog) {
            args = [options, backlog, func];
        }
        if (handle && backlog) {
            args = [handle, backlog, func];
        }
        server.listen.apply(server, args);
    });
}

export default safe;
