import {Subject} from 'rxjs';

export interface IConfigureMode {
    [name: string]: boolean;
}

export class Mode implements ReadonlyMap<string, boolean> {
    readonly store: Map<string, boolean> = new Map();
    access: boolean = false;
    readonly subject: Subject<[string, boolean]> = new Subject();

    constructor(config?: IConfigureMode) {
        if (config) {
            this.load(config);
        }
    }

    get size(): number {
        return this.store.size;
    }

    forEach(callbackfn: (value: boolean, key: string, map: ReadonlyMap<string, boolean>) => void, thisArg?: any): void {
        return this.store.forEach(callbackfn, thisArg);
    }

    get(key: string): boolean {
        return this.store.get(key);
    }

    has(key: string): boolean {
        return this.store.has(key);
    }

    [Symbol.iterator](): IterableIterator<[string, boolean]> {
        return this.store.entries();
    }

    entries(): IterableIterator<[string, boolean]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<boolean> {
        return this.store.values();
    }

    load(config: IConfigureMode): this {
        for (let name in config) {
            let access = config[name];
            if (name === '*') {
                this.access = access;
            } else {
                this.mode(name, access);
            }
        }
        return this;
    }

    enable(name: string): this {
        this.mode(name, true);
        return this;
    }

    disable(name: string): this {
        this.mode(name, false);
        return this;
    }

    isEnable(name: string): boolean {
        return this.status(name) === true;
    }

    isDisable(name: string): boolean {
        return this.status(name) === false;
    }

    clean(): this {
        this.store.clear();
        return this;
    }

    protected status(name: string): boolean {
        if (name === '*' || !this.store.has(name)) {
            return this.access;
        }
        return this.store.get(name);
    }

    protected mode(name: string, mode: boolean) {
        const status = this.store.get(name);
        if (status !== mode) {
            this.store.set(name, mode);
            this.subject.next([name, mode]);
        }
    }
}
