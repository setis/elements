import {createHash, HexBase64Latin1Encoding} from 'crypto';
import {createReadStream, lstat, writeFile} from 'fs';
import {format, parse} from 'path';
import {merge, Observable, of, Subject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {promisify} from "util";
import {SerializationCacheConfig, SerializationCacheConfigureCompile} from './serialization';

export class State<Type = string> {

    readonly subject: Subject<Type> = new Subject();
    last: Type;
    protected state: Type;

    set(state: Type) {
        if (this.state === state) {
            return;
        }
        this.last = this.state;
        this.state = state;
        this.subject.next(state);
    }

    get(): Type {
        return this.state;
    }

    async changeAsPromise(state: Type, condition: boolean) {
        if ((this.state === state) === condition) {
            return;
        }
        await this.subject
            .pipe(
                filter(value => (value === state) === condition)
            )
            .toPromise();
    }

    changeAsObservable(state: Type, condition: boolean): Observable<any> {
        return merge(
            of(this.state),
            this.subject
        )
            .pipe(
                filter(value => (value === state) === condition)
            );
    }

    asObservable() {
        return this.subject;
    }

}

export class Groups<Group = string, Element = string> {
    readonly store: Map<Group, Set<Element>> = new Map();

    isGroup(group: Group) {
        for (const name of this.store.keys()) {
            if (name == group) {
                return true;
            }
        }
        return false;
    }

    isElement(element: Element) {
        for (const elements of this.store.values()) {
            if (elements.has(element)) {
                return true;
            }
        }
        return false;
    }

    group(element: Element) {
        for (const [group, elements] of this.store) {
            if (elements instanceof Set && elements.has(element)) {
                return group;
            }
        }
    }

    element(group: Group) {
        if (this.store.has(group)) {
            return this.store.get(group);
        }
    }

    allGroup() {
        return this.store.keys();
    }

    * allElement(): IterableIterator<Element> {
        for (const names of this.store.values()) {
            for (const name of names) {
                yield name;
            }
        }
    }

    addElement(group: Group, element: Element | Element[]) {
        let container: Set<Element>;
        if (!this.store.has(group)) {
            container = new Set();
            this.store.set(group, container);
        } else {
            container = this.store.get(group);
        }
        if (element instanceof Array) {
            element.forEach(value => container.add(value));
        } else {
            container.add(element);
        }
        return this;
    }

    removeElement(group: Group, element: Element | Element[]) {
        if (!this.store.has(group)) {
            return this;
        }
        const container = this.store.get(group);
        if (element instanceof Array) {
            element.forEach(value => container.delete(value));
        } else {
            container.delete(element);
        }
        return this;
    }

    addGroup(group: Group | Group[]) {
        if (group instanceof Array) {
            group.forEach(value => {
                if (!this.store.has(value)) {
                    this.store.set(value, new Set());
                }
            });
        } else {
            if (!this.store.has(group)) {
                this.store.set(group, new Set());
            }
        }
        return this;
    }

    removeGroup(group: Group | Group[]) {
        if (group instanceof Array) {
            group.forEach(value => {
                if (this.store.has(value)) {
                    this.store.delete(value);
                }
            });
        } else {
            if (this.store.has(group)) {
                this.store.delete(group);
            }
        }
        return this;
    }

    transformGroup(data: any): Group {
        return data as Group;
    }

    transformElement(data: any): Element {
        return data as Element;
    }

    load(data: { [group: string]: Element[] }) {
        for (const group in data) {
            this.addElement(this.transformGroup(group), data[group].map(value => this.transformElement(value)));
        }
        return this;
    }

    save(): { [group: string]: Element[] } {
        const result = {};
        for (const [group, store] of this.store) {
            result[group.toString()] = Array.from(store.values());
        }
        return result;
    }

}

export class Weights<T> {
    readonly store: Map<T, number> = new Map();

    [Symbol.iterator](): IterableIterator<T> {
        return undefined;
    }

    * weights(): IterableIterator<number> {
        const result = new Set();
        for (const weight of this.store.values()) {
            if (!result.has(weight)) {
                result.add(weight);
                yield weight;
            }
        }
        result.clear();
    }

    sort(compareFn?: (a: T, b: T) => number) {
        // @ts-ignore
        const sort = Array.from(this.weights()).sort(compareFn);
        const store = new Map();

    }

}


export function TimeOfString(data: string): number {
    const regex = /(\d+)(ms|s|m|h|d)/gmi;
    if (!regex.test(data)) {
        throw new Error(`не возможно распарсить: ${data}`)
    }
    const math = regex.exec(data);
    let factor = 1;
    switch (math[2].toLowerCase()) {
        default:
        case 'ms':
            factor = 1;
            break;
        case 's':
            factor = 1e3;
            break;
        case 'm':
            factor = 60e3;
            break;
        case 'h':
            factor = 36e5;
            break;
        case 'd':
            factor = 864e5;
            break;
    }

    const number = parseInt(math[1]) * factor;
    if (isFinite(number) || isNaN(number)) {
        throw new Error(`не смог перевести в число ${data}`)
    }
    return number;
}

export function hashFile(path: string, algorithm: string, encoding?: HexBase64Latin1Encoding): string | Buffer {
    return createReadStream(path).pipe(createHash(algorithm)).digest((encoding) ? encoding : undefined);
}

export function MapToObject(data: Map<string, any>) {
    let result = {};
    for (let [k, v] of data.entries()) {
        result[k] = v;
    }
    return result;
}

export function ObjectToMap(data: object) {
    let result = new Map();
    for (let k in data) {
        result.set(k, data[k]);
    }
    return result;

}

export function cmpMap(cache: Map<string, string>, state: Map<string, string>): Set<string> {
    let keys = new Set<string>([
        ...Array.from(cache.values()),
        ...Array.from(state.values())
    ]);
    let fails = new Set<string>();
    for (let path of keys) {
        if (!cache.has(path) || !state.has(path) || state.get(path) !== cache.get(path)) {
            fails.add(path);
        }
    }
    return fails;
}

const fg = require('fast-glob');

export class SerializationCache {
    protected files: Map<string, string> = new Map();
    protected dst: string;
    protected src: string | string[];
    protected validate: number;
    protected path: string;
    protected options?: any;
    protected compile?: (src: string[] | string, dst: string, options?: any) => Promise<void>;
    protected reload: (path: string | string[]) => Promise<void>;

    constructor(config: SerializationCacheConfig) {
        const {path, src, validate = 300e3} = config;
        this.validate = validate;
        if (!path) {
            throw new Error(`не указан path`);
        }
        this.path = path;
        if (!src) {
            throw new Error(`не указан src`);
        }
        this.src = src;
        if (!config.hasOwnProperty('reload') || typeof config.reload !== "function") {
            throw new Error(`не указана или не является функцией reload`);
        }
        if (config.hasOwnProperty('compile')) {
            const {compile, dst, options} = config as SerializationCacheConfigureCompile;

            if (!dst) {
                const data = parse(this.path);
                this.dst = format({
                    root: data.root,
                    dir: data.dir,
                    name: data.name
                })
            } else {
                this.dst = dst;
            }
            if (typeof compile !== "function") {
                throw new Error(`не является функцие compile`)
            }
            if (options) {
                this.options = options;
            }
        }

    }

    async run() {
        const status = await promisify(lstat)(this.path);
        if (!status.isFile()) {
            await this.save();
        }
        await this.load();
    }

    async save() {
        const src = await fg.async(this.src) as string[];
        await this.compile(src, this.dst, this.options);
        this.files = await this.hash([this.dst, ...src]);
        await promisify(writeFile)(this.path, JSON.stringify({
            src: this.src,
            dst: this.dst,
            hash: MapToObject(this.files)
        }))
    }

    async load() {
        const config = require(this.path);
        this.src = config.src;
        this.dst = config.dst;
        this.files = ObjectToMap(config.hash);
        const hash_files = await this.hash([this.dst, ...this.src]);
        const fails = cmpMap(this.files, hash_files);
        if (fails.size === 0) {
            return;
        }
        await this.save()
    }


    protected async hash(path: string[]): Promise<Map<string, string>> {
        let result = new Map();
        path.forEach(value => {
            result.set(value, hashFile(value, 'md5', 'hex'))
        });
        return result;
    }


}
