import {isAbsolute, join, normalize, relative} from 'path';

export class Component extends Set<string> {

    constructor(public root: string, paths?: string[] | Set<string>) {
        super();
        if (paths) {
            this.load(paths);
        }
    }

    absolute(path: string) {
        return (isAbsolute(path)) ? normalize(path) : join(this.root, path);
    }

    relative(path: string) {
        return (isAbsolute(path)) ? relative(this.root, path) : path;
    }

    has(path: string) {
        const link = this.relative(path);
        return super.has(link);
    }

    add(path: string): this {
        const link = this.relative(path);
        super.add(link);
        return this;
    }

    delete(path: string) {
        const link = this.relative(path);
        return super.delete(link);
    }

    load(path: | string[] | Set<string>) {
        if (path instanceof Array) {
            path.forEach(value => {
                this.add(this.relative(value));
            });
        } else if (path instanceof Set) {
            Array.from(path).forEach(value => {
                this.add(this.relative(value));
            });
        } else {
            throw new Error(`ох , бл...`);
        }
    }

    save(absolute: boolean = true): string[] {
        return (absolute) ? Array.from(this).map(value => this.absolute(value)) : Array.from(this).map(value => this.relative(value));
    }

    toString() {
        return this.root;
    }

}
