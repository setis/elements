import {isAbsolute, join, normalize, relative} from 'path';

export class Base {
    constructor(public root: string) {

    }

    absolute(path: string) {
        return (isAbsolute(path)) ? normalize(path) : join(this.root, path);
    }

    relative(path: string) {
        return (isAbsolute(path)) ? relative(this.root, path) : path;
    }

    toString() {
        return this.root;
    }
}
