import {Component} from './component';

interface Structure {
    root: string;
    list: {
        [name: string]: {
            root: string,
            path: string[]
        }
    };
}

export class Module {
    readonly store: Map<string, Component> = new Map();
    root: string;

    has(value: string | Component) {
        switch (typeof value) {
            case 'string':
                return this.store.has(value);
            case 'object':
                if (!(value instanceof Component)) {
                    throw new Error(`ох , бл...`);
                }
                return (this.name(value) !== false);
            default:
                throw new Error(`ох , бл...`);
        }
    }

    name(value: Component): string | false {
        for (const [name, store] of this.store.entries()) {
            if (value === store) {
                return name;
            }
        }
        return false;
    }

    use(name: string) {
        if (this.store.has(name)) {
            return this.store.get(name);
        }
        return this.create(name);
    }

    remove(value: string | Component) {
        switch (typeof value) {
            case 'object':
                if (!(value instanceof Component)) {
                    throw new Error(`ох , бл...`);
                }
                const name = this.name(value);
                if (name) {
                    this.store.delete(name);
                }
                break;
            case 'string':
                if (this.store.has(value)) {
                    this.store.delete(value);
                }
                break;
            default:
                throw new Error(`ох , бл...`);
        }

    }

    create(name: string, list: string[] | Set<string> = [], root: string = this.root): Component {
        const store = new Component(root, list);
        this.store.set(name, store);
        return store;
    }

    save(): Structure {
        const result = {};
        for (const [name, component] of this.store.entries()) {
            result[name] = component.save(false);
        }
        return {
            root: this.root,
            list: result
        };
    }

    load(value: Structure) {
        const {root, list} = value;
        this.root = root;
        for (const name in list) {
            const {path, root} = list[name];
            this.create(name, path, root);
        }
    }
}
