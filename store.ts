import {Subject, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import * as uuidv4 from 'uuid/v4';

export enum StoreEnum {
    Store = 'store',
    Child = 'child'
}

export interface StoreSubject<V = any> {
    type: StoreEnum;
    id: string;
    parent?: string[];
    state: boolean;
    component?: Store<V> | V;
}

export class Store<V = any> implements ReadonlyMap<string, V> {
    readonly store: Map<string, V> = new Map();
    readonly _store_subject: Subject<[string, false] | [string, true, V]> = new Subject();
    readonly subscription: Subscription = new Subscription();
    readonly children: Map<string, Store<V>> = new Map();
    readonly _children_subject: Subject<[string, false] | [string, true, Store<V>]> = new Subject();
    readonly id: string = uuidv4();
    readonly subject: Subject<StoreSubject<V>> = new Subject();
    public relative: boolean;
    public separator: string;
    public parent?: Store<V>;
    public parseId?: (value: any) => string;
    public child: boolean;

    constructor(
        parameters: {
            parseId?: (value: any) => string,
            parent?: Store,
            separator?: string,
            relative?: boolean,
            child?: boolean
        } = {}
    ) {
        const {relative = false, separator = '/', parent, parseId, child = true} = parameters;
        this.relative = relative;
        this.parent = parent;
        this.separator = separator;
        this.parseId = parseId;
        this.child = child;

    }

    get size(): number {
        return (!this.child || this.children.size === 0)
            ? this.store.size
            : Array
                .from(this.children)
                .map(value => value[1].size)
                .reduce((previousValue, currentValue) => previousValue + currentValue);
    }

    async onInit() {
        this.subscription
            .add(
                this._store_subject.subscribe(value => {
                    const [id, state, component] = value;
                    this.subject.next({
                        type: StoreEnum.Store,
                        id,
                        state,
                        component
                    });
                })
            );
        if (!this.child) {
            this.subscription.add(
                this._children_subject.subscribe(value => {
                    const [id, state, component] = value;
                    this.subject.next({
                        type: StoreEnum.Child,
                        id,
                        state,
                        component
                    });
                })
            );
        }

        if (this.parent) {
            this.subscription.add(
                this
                    .subject
                    .pipe(
                        map(value => {
                            return {
                                ...value,
                                id: this.id,
                                parent: this.parents()
                            };
                        }))
                    .subscribe(this.parent.subject)
            );
        }
    }

    parents() {
        const result = [];
        let obj = this;
        let cil = new Set();
        while (obj.parent) {
            result.push(obj.parent.id);
            if (cil.has(obj)) {
                throw new Error(`зациклование`);
            }
            cil.add(result);
            // @ts-ignore
            obj = obj.parent;
        }
        return result.reverse();
    }

    async onDestroy() {
        for (const id of this.store.keys()) {
            this.delete(id);
        }
        if (this.children.size > 0) {
            for (const value of this.children.values()) {
                await value.onDestroy();
            }
        }
        if (this.parent) {
            this.parent.subscription.remove(this.subscription);
            this.parent.remove(this);
        }
        this.subscription.unsubscribe();
    }

    append(value: Store<V>, key: string): this {
        if (!this.child) {
            throw new Error(`запрещенно иметь детей хранилищу`);
        }
        const f = (!this.children.has(key));
        this.children.set(key, value);
        value.parent = this;
        if (f) {
            this._children_subject.next([key, true, value]);
        }
        return this;
    }

    remove(value: Store<V>): this {
        if (!this.child) {
            throw new Error(`запрещенно иметь детей хранилищу`);
        }
        for (const [id, container] of this.children) {
            if (container === value) {
                this.children.delete(id);
                this._children_subject.next([id, false]);
                break;
            }
        }
        return this;
    }

    filter(fn: (value: V, key: string) => boolean): Array<V> {
        const result = [];
        for (const [key, value] of this.store) {
            if (fn(value, key)) {
                result.push(value);
            }
        }
        if (this.child && this.children.size > 0) {
            for (const [prefix, container] of this.children) {
                result.push(...container.filter((value: V, key: string) => fn(value, `${prefix}${this.separator}${key}`)));
            }
        }
        return result;
    }

    map<T = any>(fn: (value: V, key: string) => T): Array<T> {
        const result = [];
        for (const [key, value] of this.store) {
            result.push(fn(value, key));
        }
        if (!this.child && this.children.size > 0) {
            for (const [prefix, container] of this.children) {
                result.push(...container.map((value: V, key: string) => fn(value, `${prefix}${this.separator}${key}`)));
            }
        }
        return result;
    }

    register(value: V, overwrite: boolean = false, id?: string): boolean {
        if (!id && typeof this.parseId === 'function') {
            id = this.parseId(value);
        }
        if (!id) {
            throw new Error(`не известен ключ id ${id}`);
        }
        if (this.store.has(id)) {
            if (this.store.get(id) === value) {
                return true;
            }
            if (!overwrite) {
                return false;
            }

        }
        this.set(id, value);
        return true;
    }

    unregister(name: string): this {
        if (this.store.has(name)) {
            this.delete(name);
        }
        return this;
    }

    /**
     * @todo выбор версии
     * @param name
     */
    use(name: string): V | false {
        if (this.store.has(name)) {
            return this.store.get(name);
        }
    }

    [Symbol.iterator](): IterableIterator<[string, V]> {
        return this.entries();
    }

    * entries(absolute: boolean = false): IterableIterator<[string, V]> {
        for (const entry of this.store.entries()) {
            yield entry;
        }
        if (!this.child || this.children.size > 0) {
            for (const [key, value] of this.children) {
                for (const [key2, value2] of value.entries()) {
                    yield [`${key}${this.separator}${key2}`, value2];
                }
            }
        }

    }

    forEach(callbackfn: (value: V, key: string, map: ReadonlyMap<string, V>) => void, thisArg: any = this): void {
        this.store.forEach(callbackfn, thisArg);
        if (!this.child || this.children.size > 0) {
            for (const [key, value] of this.children) {
                value.forEach((value2: V, key2: string, map: ReadonlyMap<string, V>) => callbackfn(value2, `${key}${this.separator}${key2}`, map), value);
            }
        }
    }

    parse(path: string): string[] {
        return path.split(this.separator).reverse().slice(0, 2);
    }

    get(key: string): V | undefined {
        if (!this.child || this.children.size === 0) {
            return this.store.get(key);
        }
        const [children, id] = this.parse(key);
        if (typeof id !== 'string') {
            return this.store.get(key);
        }
        for (const [key1, value] of this.children) {
            if (key1 === children) {
                return value.get(id);
            }
        }
    }

    has(key: string): boolean {
        if (!this.child || this.children.size === 0) {
            return this.store.has(key);
        }
        const [children, id] = this.parse(key);
        if (typeof id !== 'string') {
            return this.store.has(key);
        }
        for (const [k, container] of this.children) {
            if (children === k) {
                return container.has(id);
            }
        }
        return false;

    }

    * keys(): IterableIterator<string> {
        for (const key of this.store.keys()) {
            yield key;
        }
        if (this.child && this.children.size > 0) {
            for (const [key, container] of this.children) {
                for (const key2 of container.keys()) {
                    yield `${key}${this.separator}${key2}`;
                }
            }
        }
    }

    * values(): IterableIterator<V> {
        for (const container of this.store.values()) {
            yield container;
        }
        if (this.child && this.children.size > 0) {
            for (const container of this.children.values()) {
                for (const value of container.values()) {
                    yield value;
                }
            }
        }
    }

    protected set(id: string, value: V) {
        this.store.set(id, value);
        this._store_subject.next([id, true, value]);
    }

    protected delete(name: string) {
        this.store.delete(name);
        this._store_subject.next([name, false]);
    }

}
