import {createHash} from 'crypto';
import {isEqual} from 'lodash';

export class Profile implements ReadonlyMap<string, any> {
    readonly store: Map<string, any> = new Map();

    constructor(
        public algorithm: string = 'sha256',
        public block_size: number = 64
    ) {
    }

    get size(): number {
        return this.store.size;
    }

    id(value: object | string | number): string {
        let data: string;
        switch (typeof value) {
            case 'string':
                data = value;
                break;
            case 'object':
                if (value instanceof Array) {
                    data = value.join(',');
                } else {
                    const result = {};
                    for (const k of Object.keys(value).sort()) {
                        result[k] = value[k];
                    }
                    data = JSON.stringify(result);
                }
                break;
            case 'number':
            case 'bigint':
                data = value.toString();
                break;
            case 'boolean':
                data = JSON.stringify(value);
                break;
            case 'undefined':
                data = '';
                break;
        }
        return (data.length > this.block_size) ? createHash(this.algorithm).update(data).digest().toString() : data;
    }

    forEach(callbackfn: (value: any, key: string, map: ReadonlyMap<string, any>) => void, thisArg?: any): void {
        return this.store.forEach(callbackfn, thisArg);
    }

    get(key: string) {
        return this.store.get(key);
    }

    has(key: string): boolean {
        return this.store.has(key);
    }

    [Symbol.iterator](): IterableIterator<[string, any]> {
        return this.store.entries();
    }

    entries(): IterableIterator<[string, any]> {
        return this.store.entries();
    }

    keys(): IterableIterator<string> {
        return this.store.keys();
    }

    values(): IterableIterator<any> {
        return this.store.values();
    }

    find(config: object): string {
        for (let [name, options] of this.store) {
            if (isEqual(options, config)) {
                return name;
            }
        }
    }

    check(value: any): boolean {
        const id = this.id(value);
        return this.store.has(id);
    }

    use(value: any): any {
        const id = this.id(value);
        if (this.store.has(id)) {
            return this.store.get(id);
        }
    }

    add(value: object | string | number, overwrite: boolean = false): this {
        const id = this.id(value);
        if (this.store.has(id) && !overwrite) {
            throw new Error(`уже существует id: ${id}`);
        }
        this.store.set(id, value);
        return this;
    }

    remove(value: object | string | number): this {
        const id = this.id(value);
        if (this.store.has(id)) {
            this.store.delete(id);
        }
        return this;
    }

    toObject() {
        let result = {};
        for (let [name, config] of this.store) {
            result[name] = config;
        }
        return result;
    }

    fromObject(value: object): this {
        for (const name in value) {
            this.store.set(name, value[name]);
        }
        return this;
    }

    fromJson(value: string): this {
        const data = JSON.parse(value);
        return this.fromObject(data);
    }


    toJson() {
        return JSON.stringify(this.toObject());
    }

}

export class Profiles {
    readonly store: Map<string, Profile> = new Map();

    constructor(
        public plode: string = ':'
    ) {
    }

    query(value: string): any {
        const [name, id] = value.split(this.plode, 2);
        if (!this.store.has(name)) {
            return;
        }
        const profile = this.store.get(name);
        return profile.use(id);
    }

    use(prefix: string) {
        if (this.store.has(prefix)) {
            return this.store.get(prefix);
        }
        return this.create(prefix);
    }

    create(prefix: string) {
        const profile = new Profile();
        this.store.set(prefix, profile);
        return profile;
    }

    remove(prefix: string): this {
        if (this.store.has(prefix)) {
            this.store.delete(prefix);
        }
        return this;
    }

    toObject() {
        const result = {};
        for (const [name, profile] of this.store) {
            result[name] = profile.toObject();
        }
        return result;
    }

    toJson() {
        return JSON.stringify(this.toObject());
    }

}
