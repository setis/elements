import {readdir} from 'fs';
import {join} from 'path';
import {Subject, Subscription} from 'rxjs';
import {promisify} from 'util';
import {Property as ConfigProperty} from './configure';
import {PathComponent} from './path';
import {ActionEnum, RegistryOnce} from './registry';
import {IMeta, IStructure} from '../system/interface';

const log = require('debug')('elements:loader');
export const logger = {
    error: log.extend('error'),
    warn: log.extend('warn'),
    info: log.extend('info'),
    log: log.extend('log'),
    debug: log.extend('debug')
};


export interface IConfigureLoader {
    path?: string[];
    package?: {
        [name: string]: {
            require: string,
            parameters?: Array<any>
        } | string
    }
    registry?: {
        [name: string]: string;
    };
    mode: string[];
}

export class Loader<Meta extends IMeta = any, Structure extends IStructure = any> {

    registry: RegistryOnce;
    package: string[] = [];
    readonly loading: Set<string> = new Set();
    path: PathComponent;
    readonly subscription: Subscription = new Subscription();
    parent?: Loader;
    config?: ConfigProperty<IConfigureLoader>;
    readonly mode: Set<string> = new Set(['path', 'registry', 'package']);
    readonly subject: Subject<{
        meta: any,
        path?: string
    }> = new Subject();

    constructor(parameters: {
        config?: ConfigProperty<IConfigureLoader>,
        path: PathComponent | string,
        parent?: Loader
    }) {
        const {config, parent, path} = parameters;

        if (config) {
            this.config = config;
        }
        if (parent) {
            this.parent = parent;
        }
        switch (typeof path) {
            case 'string':
                this.path = new PathComponent(path);
                break;
            case 'object':
                if (!(path instanceof PathComponent)) {
                    throw new Error();
                }
                this.path = path;
                break;
            default:
                throw new Error();
        }

    }

    async onInit() {
        this.registry = new RegistryOnce({root: this.path.root});
        if (this.parent) {
            const {registry, loading} = this.parent;
            const subscription = this.registry.subject.subscribe(value => {
                switch (value.action) {
                    case ActionEnum.Change: {
                        const {name, link} = value;
                        if (registry.has(link)) {
                            registry.add(link, name);
                        }
                        loading.add(link);
                    }
                        break;
                    case ActionEnum.Delete: {
                        const {link} = value;
                        registry.remove(link);
                        loading.delete(link);
                    }
                        break;
                    case ActionEnum.Insert: {
                        const {name, link} = value;
                        registry.set(link, name);
                        loading.add(link);
                    }
                        break;
                }
            });
            this.registry.subscription.add(subscription);
        }
        this.subscription.add(this.registry.subscription);
        if (this.config) {
            this.path.load(this.config.get('path', []));
            this.registry.load(this.config.get('registry', {}));
            this.package = this.config.get('package', []);
            this.mode.clear();
            this
                .config
                .get<string[]>('mode', ['path', 'registry', 'package'])
                .forEach(value => this.mode.add(value));
        }
    }

    async loader(): Promise<void> {
        for (const name of this.mode) {
            await this[`loader_${name}`]();
        }
    }

    async loader_path() {
        if (this.path) {
            await Promise.all(
                this.path.save(true).map(value => this.dir(value))
            );
        }
    }

    async loader_registry() {
        if (this.registry) {
            await Promise.all(
                this.registry
                    .paths(true)
                    .filter(value => !this.loading.has(value))
                    .map(this.file.bind(this))
            );
        }

    }

    /**
     * @todo если package object  например
     * package:
     favicon: "koa@1"
     */
    async loader_package() {
        if (this.package) {
            await Promise.all(
                this.package
                    .filter(value => {
                        let path;
                        try {
                            path = require.resolve(value);
                        } catch (e) {
                            return false;
                        }
                        return !this.loading.has(path);
                    })
                    .map(this.file.bind(this))
            );
        }
    }

    async dir(dir: string): Promise<void> {
        let list: string[] = [];
        try {
            list = await promisify(readdir)(dir);
        } catch (e) {
            if (e.code === 'ENOENT') {
                logger.error(`action: dir path: ${this.path.relative(dir)} link: ${dir} msg: не существует папки`);
            } else {
                logger.error(`action: dir path: ${this.path.relative(dir)} link: ${dir}`, e);
            }
            logger.info(`action: dir path: ${this.path.relative(dir)}`)
        }

        await Promise.all(
            list
                .map(file => join(dir, file))
                .filter(value => !this.loading.has(value))
                .map(file => this.file(file))
        );
        logger.info(`action: dir names: ${list.map(value => this.path.relative(value)).join(',')} path: ${this.path.relative(dir)}`);
    }

    async file(path: string): Promise<void> {
        try {
            const module = require(path);
            const link = require.resolve(path);
            if (link !== path) {
                this.loading.add(link);
            }
            this.loading.add(path);
            this.subject.next({
                meta: module,
                path
            });
        } catch (e) {
            if (e.constructor.name === 'Error' && /Cannot find module.*?/gm.test(e.message)) {
                logger.warn(`action: file msg: проверти существования папки path: ${this.path.relative(path)} link: ${path}`);
            } else {
                logger.error(`action: file name: ${e.name} msg: ${e.message} path: ${this.path.relative(path)} link: ${path}`, e.stack);
            }
            return;
        }
        logger.info(`action: file path: ${this.path.relative(path)}`);
    }

    async onDestroy() {
        this.subscription.unsubscribe();

    }
}
