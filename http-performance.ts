import {Context, Middleware} from 'koa';

export interface PerformanceHistory {
    id: any
    time: Date

    [x: string]: any
}

declare module 'koa' {
    interface Context {
        performance: Array<PerformanceHistory>

    }
}

export function http_performance(id: any, middleware: Middleware): Middleware {
    return async (ctx: Context, next: () => Promise<any>) => {
        ctx.performance.push({
            id,
            time: new Date()
        });
        await middleware(ctx, next);
    };

}
