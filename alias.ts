export class Alias extends Map<string, string> {

    constructor(
        configure?: { [name: string]: string }
    ) {
        super();
        if (configure) {
            this.load(configure);
        }

    }

    load(value: { [name: string]: string }): this {
        for (const name in value) {
            this.set(name, value[name]);
        }
        return this;
    }

    use(path: string): string {
        let current = path;
        let prev;
        const uniq = new Set();
        while (current !== undefined && this.has(current)) {
            if (uniq.has(current)) {
                throw new Error(` бесконечный цикл ${prev} - ${current}`);
            }
            uniq.add(current);
            prev = current;
            current = this.get(current);
        }
        if (current === path) {
            throw new Error(`нет альтернативы ${path}`);
        }
        return current;
    }
}
